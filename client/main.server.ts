import { enableProdMode } from '@angular/core';

import { environment } from './environments/environment';

(global as any).WebSocket = require('ws');
(global as any).XMLHttpRequest = require('xhr2');

if (environment.production) {
    enableProdMode();
}

export { AppServerModule } from './app/app.server.module';
export { renderModule, renderModuleFactory } from '@angular/platform-server';
