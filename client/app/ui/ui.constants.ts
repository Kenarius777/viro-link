export enum CenteredPageSize {
    LARGE = 'large',
    MEDIUM = 'medium',
    SMALL = 'small'
}

export const CENTERED_PAGE_SIZE_WIDTH_LIST: {
    [Property in CenteredPageSize]: number
} = {
    [CenteredPageSize.SMALL]: 600,
    [CenteredPageSize.MEDIUM]: 750,
    [CenteredPageSize.LARGE]: 900,
};

export const DEFAULT_CENTERED_PAGE_SIZE = CenteredPageSize.SMALL;
