export interface IInlineSelectorItem {
    key: string;
    value: string;
}
