export interface IQRState {
    url: string;
    size: number;
    margin: number;
    errorCorrectionLevel: 'L' | 'M' | 'Q' | 'H';
    colorDark: string;
    colorLight: string;
}
