import { NgModule } from '@angular/core';
import { InlineSelectorComponent } from './components/inline-selector/inline-selector.component';
import { CommonModule } from '@angular/common';
import { MatRippleModule } from '@angular/material/core';
import { LinkBarComponent } from './components/link-bar/link-bar.component';
import { LinkBarItemComponent } from './components/link-bar/link-bar-item.component';
import { RouterModule } from '@angular/router';
import { LogoComponent } from './components/logo/logo.component';
import { QRCodeComponent } from './components/qr-code/qr-code.component';
import { BackButtonDirective } from './directives';
import { InlineButtonComponent } from './components/inline-button/inline-button.component';
import { CenteredPageLayoutComponent } from './layouts/centered-page-layout/centered-page-layout.component';
import { StoreModule } from '@ngrx/store';
import { UI_FEATURE_KEY } from './store/ui.state';
import { uiReducer } from './store/ui.reducers';
import { CenteredPageSizeService } from './services';


@NgModule({
    declarations: [
        InlineSelectorComponent,
        LinkBarComponent,
        LinkBarItemComponent,
        LogoComponent,
        QRCodeComponent,
        BackButtonDirective,
        InlineButtonComponent,
        CenteredPageLayoutComponent,
    ],
    imports: [
        CommonModule,
        MatRippleModule,
        RouterModule,
        StoreModule.forFeature(UI_FEATURE_KEY, uiReducer)
    ],
    providers: [
        CenteredPageSizeService
    ],
    exports: [
        InlineSelectorComponent,
        LinkBarComponent,
        LinkBarItemComponent,
        LogoComponent,
        QRCodeComponent,
        BackButtonDirective,
        InlineButtonComponent,
    ]
})
export class UIModule {
}
