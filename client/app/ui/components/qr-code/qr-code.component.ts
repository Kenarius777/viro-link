import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil, filter } from 'rxjs/operators';
import { isEqual as _isEqual } from 'lodash';
import QRCode from 'qrcode';
import { IQRState } from '../../interfaces';

@Component({
    selector: 'ui-qr-code',
    templateUrl: './qr-code.component.html',
    styleUrls: ['./qr-code.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class QRCodeComponent implements OnInit, OnDestroy {
    private readonly defaultState: IQRState = {
        url: '',
        size: 150,
        margin: 0,
        errorCorrectionLevel: 'M',
        colorDark: '#000000ff',
        colorLight: '#ffffffff'
    };

    private readonly qrState$ = new BehaviorSubject(this.defaultState);

    private readonly unsubscribe$: Subject<void> = new Subject<void>();

    qr: string | undefined;

    @Input() set link(value: string) {
        this.updateState({ url: value });
    }

    @Input() set size(value: number) {
        this.updateState({ size: +value });
    }

    @Input() set margin(value: number) {
        this.updateState({ margin: +value });
    }

    @Input() set errorCorrectionLevel(value: 'L' | 'M' | 'Q' | 'H') {
        this.updateState({ errorCorrectionLevel: value });
    }

    @Input() set colorDark(value: string) {
        this.updateState({ colorDark: value });
    }

    @Input() set colorLight(value: string) {
        this.updateState({ colorLight: value });
    }

    constructor(private cdr: ChangeDetectorRef) {
    }

    ngOnInit() {
        this.qrState$.pipe(
            distinctUntilChanged(_isEqual),
            debounceTime(100),
            filter((state: IQRState): boolean => {
                return !!state.url;
            }),
            takeUntil(this.unsubscribe$)
        ).subscribe((state: IQRState) => {
            this.generateQr(state);
        });
    }

    private async generateQr(state: IQRState) {
        this.qr = await QRCode.toDataURL(state.url, {
            width: state.size,
            margin: state.margin,
            errorCorrectionLevel: state.errorCorrectionLevel,
            color: {
                dark: state.colorDark,
                light: state.colorLight
            }
        });
        this.cdr.detectChanges();
    }

    private updateState(state) {
        const newState = Object.assign({}, this.qrState$.value, state);
        this.qrState$.next(newState);
    }

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
}

