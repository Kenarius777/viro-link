import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { IInlineSelectorItem } from '../../interfaces';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'ui-inline-selector',
    templateUrl: './inline-selector.component.html',
    styleUrls: ['./inline-selector.component.less'],
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => InlineSelectorComponent),
        multi: true
    }]
})
export class InlineSelectorComponent implements OnInit, ControlValueAccessor {

    @Input() itemList: ReadonlyArray<IInlineSelectorItem> = [];

    get value(): string | null {
        return this.innerValue;
    }
    set value(item: string | null) {
        this.innerValue = item;
        this.onChangeCallback(item);
    }

    private innerValue: string | null = null;

    private onChangeCallback: (_: any) => void = () => {};
    private onTouchCallback: (_: any) => void = () => {};

    constructor() {
    }

    ngOnInit(): void {
    }

    setSelected(item: string | null) {
        this.value = item;
    }

    registerOnChange(fn: any): void {
        this.onChangeCallback = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouchCallback = fn;
    }

    writeValue(obj: any): void {
        this.value = obj;
    }
}
