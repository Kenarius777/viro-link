import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'ui-inline-button',
    templateUrl: './inline-button.component.html',
    styleUrls: ['./inline-button.component.less']
})
export class InlineButtonComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}
