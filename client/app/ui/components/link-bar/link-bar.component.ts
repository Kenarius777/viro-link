import { Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { LinkBarItemComponent } from './link-bar-item.component';

@Component({
    selector: 'ui-link-bar',
    templateUrl: './link-bar.component.html',
    styleUrls: ['./link-bar.component.less']
})
export class LinkBarComponent implements OnInit {

    @ContentChildren(LinkBarItemComponent)
    items!: QueryList<LinkBarItemComponent>;

    constructor() {
    }

    ngOnInit(): void {
    }

}
