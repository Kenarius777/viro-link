import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'ui-link-bar-item',
    template: ''
})
export class LinkBarItemComponent implements OnInit {

    @Input() label: string = '';
    @Input() link: string = '';

    constructor() {
    }

    ngOnInit(): void {
    }

}
