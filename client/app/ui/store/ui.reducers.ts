import { Action, createReducer, on } from '@ngrx/store';
import { UI_DEFAULT_STATE, IUIState } from './ui.state';
import { uiSetPageSize } from './ui.actions';

const reducer = createReducer(
    UI_DEFAULT_STATE,
    on(uiSetPageSize, (state, { pageSize }) => ({ ...state, centeredPageSize: pageSize }))
);

export function uiReducer(state: IUIState | undefined, action: Action): IUIState {
    return reducer(state, action);
}
