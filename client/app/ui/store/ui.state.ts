import { CenteredPageSize, DEFAULT_CENTERED_PAGE_SIZE } from '../ui.constants';

export const UI_FEATURE_KEY = 'ui';

export interface IUIState {
    centeredPageSize: CenteredPageSize;
}

export const UI_DEFAULT_STATE: IUIState = {
    centeredPageSize: DEFAULT_CENTERED_PAGE_SIZE
};
