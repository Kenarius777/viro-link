import { createAction, props, union } from '@ngrx/store';
import { CenteredPageSize } from '../ui.constants';

export enum UI_ACTIONS {
    SET_PAGE_SIZE = '[UI] set page size'
}

export const uiSetPageSize = createAction(
    UI_ACTIONS.SET_PAGE_SIZE,
    props<{ pageSize: CenteredPageSize}>()
);

const uiActionsUnion = union({
    uiSetPageSize
});

export type UIActions = typeof uiActionsUnion;
