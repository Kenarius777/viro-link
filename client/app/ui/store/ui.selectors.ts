import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UI_FEATURE_KEY, IUIState } from './ui.state';

export const uiStateSelector = createFeatureSelector<IUIState>(UI_FEATURE_KEY);

export const centeredPageSizeSelector = createSelector(
    uiStateSelector,
    (state: IUIState) => state.centeredPageSize
);
