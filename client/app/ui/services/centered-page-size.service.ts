import { Injectable } from '@angular/core';
import { IUIState } from '../store/ui.state';
import { Store } from '@ngrx/store';
import { uiSetPageSize } from '../store/ui.actions';
import { CenteredPageSize } from '../ui.constants';

@Injectable()
export class CenteredPageSizeService {

    constructor(private store: Store<IUIState>) {}

    setPageSize(nextSize: CenteredPageSize) {
        this.store.dispatch(uiSetPageSize({ pageSize: nextSize }));
    }
}
