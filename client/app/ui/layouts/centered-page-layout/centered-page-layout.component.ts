import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { CenteredPageSizeService } from '../../services';
import * as seedrandom from 'seedrandom';
import { DateUtils } from '../../../shared/utils';
import { IUIState } from '../../store/ui.state';
import { select, Store } from '@ngrx/store';
import { centeredPageSizeSelector } from '../../store/ui.selectors';
import { CenteredPageSize, DEFAULT_CENTERED_PAGE_SIZE } from '../../ui.constants';

@Component({
    selector: 'ui-centered-page-layout-component',
    templateUrl: './centered-page-layout.component.html',
    styleUrls: ['./centered-page-layout.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CenteredPageLayoutComponent implements OnInit, OnDestroy {

    pageSize: CenteredPageSize = DEFAULT_CENTERED_PAGE_SIZE;

    get pageSizeClass(): string {
        switch (this.pageSize) {
            case CenteredPageSize.LARGE:
                return 'centered-page-large';
            case CenteredPageSize.MEDIUM:
                return 'centered-page-medium';
            case CenteredPageSize.SMALL:
                return 'centered-page-small';
        }
    }

    currentYear: string = DateUtils.yyyy(new Date());
    backgroundImageIndex!: number;

    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        private pageSizeService: CenteredPageSizeService,
        private store: Store<IUIState>,
        private router: Router,
        private cdr: ChangeDetectorRef,
    ) {
    }

    ngOnInit(): void {
        this.pageSizeService.setPageSize(DEFAULT_CENTERED_PAGE_SIZE);
        this.initRouterEventsListener();
        this.initPageSizeListener();
        this.generateTodayBackgroundImageIndex();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    generateTodayBackgroundImageIndex() {
        const date = DateUtils.yyyymmdd(new Date());
        const min = 1;
        const max = 31;
        const rng = seedrandom(date);
        this.backgroundImageIndex = Math.floor(rng() * (max - min) + min);
        this.cdr.detectChanges();
    }

    private initRouterEventsListener(): void {
        this.router.events.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(navigationEvent => {
            if (navigationEvent instanceof NavigationEnd) {
                this.pageSizeService.setPageSize(DEFAULT_CENTERED_PAGE_SIZE);
            }
        });
    }

    private initPageSizeListener(): void {
        this.store.pipe(select(centeredPageSizeSelector)).pipe(
            distinctUntilChanged(),
            takeUntil(this.unsubscriber$)
        ).subscribe(nextSize => {
            this.pageSize = nextSize;
            this.cdr.detectChanges();
        });
    }
}
