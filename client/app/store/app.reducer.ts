import {
    Action,
    ActionReducer,
    ActionReducerMap,
    createReducer,
    MetaReducer,
    on
} from '@ngrx/store';
import {storeFreeze} from 'ngrx-store-freeze';

import { environment } from '../../environments/environment';
import { IAppState, IAppGlobalState, APP_GLOBAL_DEFAULT_STATE } from './app.state';
import { appSetTitle } from './app.actions';

export const reducers: ActionReducerMap<IAppState> = {
    app: appReducer
};

const appGlobalReducer = createReducer(
    APP_GLOBAL_DEFAULT_STATE,
    on(appSetTitle, (state, { title }) => ({ ...state, title }))
);

export function appReducer(state: IAppGlobalState | undefined, action: Action): IAppGlobalState {
    return appGlobalReducer(state, action);
}

export function storeLogger(actionReducer: ActionReducer<IAppState>): ActionReducer<IAppState> {
    return (state: IAppState, action: Action): IAppState => {
        const nextState = actionReducer(state, action);
        console.groupCollapsed('action', action.type);
        // tslint:disable-next-line:no-console
        console.log('action', action);
        // tslint:disable-next-line:no-console
        console.log('current state', state);
        // tslint:disable-next-line:no-console
        console.log('next state', nextState);
        console.groupEnd();

        return nextState;
    };
}

export const metaReducers: Array<MetaReducer<IAppState>> = [];

if (!environment.production) {
    metaReducers.push(storeFreeze);

    if (environment.storeLogger) {
        metaReducers.push(storeLogger);
    }
}
