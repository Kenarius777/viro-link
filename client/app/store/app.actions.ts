import { createAction, props, union } from '@ngrx/store';

export enum APP_ACTIONS {
    SET_TITLE = '[APP] set title'
}

export const appSetTitle = createAction(
    APP_ACTIONS.SET_TITLE,
    props<{ title: string }>()
);

const appActionUnion = union({
    appSetTitle
});

export type AppActions = typeof appActionUnion;
