import { IAppGlobalState, IAppState } from './app.state';
import { createSelector } from '@ngrx/store';

export const appGlobalSelector = (appState: IAppState) => appState.app;

export const appTitleSelector = createSelector(
    appGlobalSelector,
    (state: IAppGlobalState) => state.title
);
