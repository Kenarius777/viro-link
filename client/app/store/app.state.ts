import { environment } from '../../environments/environment';

export interface IAppState {
    app: IAppGlobalState;
}

export interface IAppGlobalState {
    title: string;
}

export const APP_GLOBAL_DEFAULT_STATE: IAppGlobalState = {
    title: environment.serviceName,
};

export const APP_DEFAULT_STATE: IAppState = {
    app: APP_GLOBAL_DEFAULT_STATE,
};
