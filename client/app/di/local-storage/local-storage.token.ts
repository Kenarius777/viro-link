import { ILocalStorage } from './interfaces';
import { InjectionToken } from '@angular/core';

export const LOCAL_STORAGE = new InjectionToken<ILocalStorage>('Local storage');
