import { localStorageFactory } from './local-storage.factory';
import { PLATFORM_ID, Provider } from '@angular/core';
import { LOCAL_STORAGE } from './local-storage.token';

export let localStorageProvider: Provider = {
    provide: LOCAL_STORAGE,
    useFactory: localStorageFactory,
    deps: [PLATFORM_ID]
};
