import { isPlatformBrowser } from '@angular/common';
import { MockLocalStorage } from './models';
import { ILocalStorage } from './interfaces';

export function localStorageFactory(platformId: Object): ILocalStorage {
    return isPlatformBrowser(platformId) ? localStorage : new MockLocalStorage();
}
