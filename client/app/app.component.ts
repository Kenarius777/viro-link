import { Component, OnDestroy, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { IAppState } from './store/app.state';
import { select, Store } from '@ngrx/store';
import { appTitleSelector } from './store/app.selectors';
import { Title } from '@angular/platform-browser';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
    title = environment.serviceName;

    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        private store: Store<IAppState>,
        private titleService: Title
    ) {
    }

    ngOnInit(): void {
        this.store.pipe(
            select(appTitleSelector)
        ).subscribe(title => {
            this.titleService.setTitle(title);
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }
}
