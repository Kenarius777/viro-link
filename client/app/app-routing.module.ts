import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { CenteredPageLayoutComponent } from './ui/layouts/centered-page-layout/centered-page-layout.component';
import { ErrorComponent } from './shared/components/error/error.component';
import { AboutComponent } from './pages/about/about.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/shorten/link',
        pathMatch: 'full'
    },
    {
        path: 'shorten',
        component: CenteredPageLayoutComponent,
        loadChildren: () => import('./short-link/short-link.module').then(m => m.ShortLinkModule)
    },
    {
        path: 'about',
        component: CenteredPageLayoutComponent,
        children: [
            {
                path: '',
                component: AboutComponent
            }
        ]
    },
    {
        path: '404',
        component: NotFoundComponent
    },
    {
        path: 'error',
        component: ErrorComponent
    },
    {
        path: ':code',
        component: CenteredPageLayoutComponent,
        loadChildren: () => import('./short-link-content/short-link-content.module').then(m => m.ShortLinkContentModule)
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {
        initialNavigation: 'enabled',
        enableTracing: false
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
