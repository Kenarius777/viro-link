import { NgModule } from '@angular/core';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './pages/about/about.component';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UIModule } from './ui/ui.module';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './store/app.reducer';
import { EffectsModule } from '@ngrx/effects';
import { APP_DEFAULT_STATE } from './store/app.state';
import { APP_ICONS } from './app.icons';
import { MatButtonModule } from '@angular/material/button';
import { localStorageProvider } from './di/local-storage/local-storage.provider';

@NgModule({
    declarations: [
        AppComponent,
        AboutComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'serverApp'}),
        BrowserTransferStateModule,
        AppRoutingModule,
        SharedModule,
        BrowserAnimationsModule,
        UIModule,
        FontAwesomeModule,
        ServiceWorkerModule.register('ngsw-worker.js', {
            enabled: environment.pwaEnabled,
            // Register the ServiceWorker as soon as the app is stable
            // or after 30 seconds (whichever comes first).
            registrationStrategy: 'registerWhenStable:30000'
        }),
        StoreModule.forRoot(reducers, {
            initialState: APP_DEFAULT_STATE,
            metaReducers: metaReducers
        }),
        EffectsModule.forRoot([]),
        MatButtonModule,
    ],
    providers: [
        localStorageProvider,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(library: FaIconLibrary) {
        library.addIcons(...APP_ICONS);
    }
}
