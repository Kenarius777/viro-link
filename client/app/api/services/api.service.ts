import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
    IDeleteShortLinkRequest,
    ISavedShortLinkResponse,
    ISaveLinkDataRequest,
    ISaveTextDataRequest,
    IShortLinkContentResponse
} from '../interfaces';
import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService implements OnDestroy {
    private apiBaseUrl = `${environment.apiUrl}/api`;

    constructor(private http: HttpClient) {
    }

    ngOnDestroy(): void {
        throw new Error('Method not implemented.');
    }

    getLinkContent(code: string): Observable<IShortLinkContentResponse> {
        return this.http.get<IShortLinkContentResponse>(
            `${this.apiBaseUrl}/short-link/${code}`
        );
    }

    saveLinkData(request: ISaveLinkDataRequest): Observable<ISavedShortLinkResponse> {
        return this.http.post<ISavedShortLinkResponse>(
            `${this.apiBaseUrl}/short-link/link/save`,
            request
        );
    }

    saveTextData(request: ISaveTextDataRequest): Observable<ISavedShortLinkResponse> {
        return this.http.post<ISavedShortLinkResponse>(
            `${this.apiBaseUrl}/short-link/text/save`,
            request
        );
    }

    deleteShortLink(code: string, request: IDeleteShortLinkRequest): Observable<boolean> {
        return this.http.request<boolean>(
            'delete',
            `${this.apiBaseUrl}/short-link/${code}`,
            {
                body: request
            }
        );
    }

}
