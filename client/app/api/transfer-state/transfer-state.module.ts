import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserTransferHttpResponseInterceptor, ServerTransferHttpResponseInterceptor } from './interceptors';

@NgModule({
    declarations: [],
    imports: [],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ServerTransferHttpResponseInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BrowserTransferHttpResponseInterceptor,
            multi: true
        }
    ],
    exports: []
})
export class TransferStateModule {
}
