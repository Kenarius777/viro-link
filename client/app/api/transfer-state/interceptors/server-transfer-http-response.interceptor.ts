import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { TRANSFER_HTTP_RESPONSE_KEY_PREFIX } from '../../api.constants';
import { isPlatformServer } from '@angular/common';
import { tap } from 'rxjs/operators';
import { IHttpResponseState } from '../interfaces';

@Injectable()
export class ServerTransferHttpResponseInterceptor implements HttpInterceptor {

    constructor(
        private transferState: TransferState,
        @Inject(PLATFORM_ID) private platformId: Object
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method !== 'GET') {
            return next.handle(request);
        }

        if (isPlatformServer(this.platformId)) {
            const key = makeStateKey(TRANSFER_HTTP_RESPONSE_KEY_PREFIX + request.url);
            return next.handle(request).pipe(
                tap(response => {
                    if (response instanceof HttpResponse && response.status === 200) {
                        this.transferState.set<IHttpResponseState>(key, {
                            body: response.body
                        });
                    }
                })
            );
        }

        return next.handle(request);
    }
}
