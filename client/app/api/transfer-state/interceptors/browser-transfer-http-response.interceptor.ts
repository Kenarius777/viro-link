import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { makeStateKey, TransferState } from '@angular/platform-browser';
import { TRANSFER_HTTP_RESPONSE_KEY_PREFIX } from '../../api.constants';
import { isPlatformBrowser } from '@angular/common';
import { IHttpResponseState } from '../interfaces';

@Injectable()
export class BrowserTransferHttpResponseInterceptor implements HttpInterceptor {

    constructor(
        private transferState: TransferState,
        @Inject(PLATFORM_ID) private platformId: Object
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method !== 'GET') {
            return next.handle(request);
        }

        if (isPlatformBrowser(this.platformId)) {
            const key = makeStateKey(TRANSFER_HTTP_RESPONSE_KEY_PREFIX + request.url);
            const cachedResponse = this.transferState.get<IHttpResponseState | null>(key, null);
            if (cachedResponse) {
                this.transferState.remove(key);
                return of(
                    new HttpResponse({
                        status: 200,
                        statusText: 'OK',
                        body: cachedResponse.body,
                    })
                );
            }
        }

        return next.handle(request);
    }
}
