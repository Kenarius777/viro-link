export interface ISaveLinkDataRequest {
    shortCode: string | null;
    link: string;
}
