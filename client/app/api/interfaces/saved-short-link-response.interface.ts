export interface ISavedShortLinkResponse {
    code: string;
    shortLink: string;
    deletionKey: string;
    preview: ILinkDataPreviewResponse | ITextDataPreviewResponse;
}

export interface ILinkDataPreviewResponse {
    link: string;
}

export interface ITextDataPreviewResponse {
    text: string;
}
