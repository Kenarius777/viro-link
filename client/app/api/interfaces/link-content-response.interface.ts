export interface IShortLinkContentResponse {
    type: 'link' | 'text';
    data: IShortLinkContentLinkDataResponse | IShortLinkContentTextDataResponse;
}

export interface IShortLinkContentLinkDataResponse {
    link: string;
}

export interface IShortLinkContentTextDataResponse {
    text: string;
    pageSize: string;
    textType: string;
}
