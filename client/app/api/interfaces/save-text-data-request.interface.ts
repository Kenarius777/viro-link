export interface ISaveTextDataRequest {
    shortCode: string | null;
    text: string;
    pageSize: string;
    textType: string;
}
