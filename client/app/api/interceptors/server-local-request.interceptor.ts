import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { isPlatformServer } from '@angular/common';
import { environment } from '../../../environments/environment';

@Injectable()
export class ServerLocalRequestInterceptor implements HttpInterceptor {

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (isPlatformServer(this.platformId) && environment.localApiUrl) {
            const requestWithLocalUrl = request.clone(
                {
                    url: request.url.replace(environment.apiUrl, environment.localApiUrl)
                }
            );
            return next.handle(requestWithLocalUrl);
        }

        return next.handle(request);
    }
}
