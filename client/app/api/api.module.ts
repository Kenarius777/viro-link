import { NgModule } from '@angular/core';
import { ApiService } from './services';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TransferStateModule } from './transfer-state/transfer-state.module';
import { ServerLocalRequestInterceptor } from './interceptors';

@NgModule({
    declarations: [],
    imports: [
        HttpClientModule,
        TransferStateModule,
    ],
    providers: [
        ApiService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ServerLocalRequestInterceptor,
            multi: true
        }
    ],
    exports: []
})
export class ApiModule {
}
