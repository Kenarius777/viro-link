import { AbstractControl, ValidationErrors } from '@angular/forms';

export function shortCodeValidator(control: AbstractControl): ValidationErrors | null {
    const urlRegex = /^[a-zA-Z0-9]{3,64}/;
    const isValid = () => urlRegex.test(control.value);
    return !control.value || isValid() ? null : { shortcode: true };
}
