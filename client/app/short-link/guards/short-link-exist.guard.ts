import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ShortLinkService } from '../services';
import { catchError } from 'rxjs/operators';
import { ErrorPageNavigateService } from '../../shared/services';

@Injectable()
export class ShortLinkExistGuard implements CanActivate {

    constructor(
        private shortLinkService: ShortLinkService,
        private errorPageNavigateService: ErrorPageNavigateService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        const linkCode = route.params.code;
        return this.shortLinkService.isLinkExist(linkCode).pipe(
            catchError((e) => {
                this.errorPageNavigateService.resolveAndNavigate(e, state.url);
                throw e;
            })
        );
    }
}
