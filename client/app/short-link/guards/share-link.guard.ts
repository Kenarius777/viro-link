import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ShareTargetDataService, ShortLinkService } from '../services';
import { catchError, map } from 'rxjs/operators';
import { ErrorPageNavigateService } from '../../shared/services';

@Injectable()
export class ShareLinkGuard implements CanActivate {

    constructor(
        private shortLinkService: ShortLinkService,
        private shareTargetDataService: ShareTargetDataService,
        private errorPageNavigateService: ErrorPageNavigateService,
        private router: Router
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.shareTargetDataService.shareData(route.queryParams.title, route.queryParams.text, route.queryParams.url).pipe(
            map(shortLink => {
                this.router.navigate(['/shorten/result', shortLink.code]);
                return false;
            }),
            catchError((e) => {
                this.errorPageNavigateService.resolveAndNavigate(e, state.url);
                throw e;
            })
        );
    }
}
