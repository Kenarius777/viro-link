import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IShortLinkInfo } from '../../interfaces';
import { NavigationStart, Router } from '@angular/router';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
    templateUrl: './shorten-link-result-dialog.component.html',
    styleUrls: ['./shorten-link-result-dialog.component.less']
})
export class ShortenLinkResultDialogComponent implements OnInit, OnDestroy {

    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IShortLinkInfo,
        private router: Router,
        private dialogRef: MatDialogRef<ShortenLinkResultDialogComponent>
    ) {
    }

    ngOnInit(): void {
        this.router.events.pipe(
            filter(event => event instanceof NavigationStart),
            takeUntil(this.unsubscriber$)
        ).subscribe(() => {
            this.dialogRef.close();
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

}
