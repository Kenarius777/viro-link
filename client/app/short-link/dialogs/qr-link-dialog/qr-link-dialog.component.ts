import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IQrLinkDialogData } from '../../interfaces';

@Component({
    templateUrl: './qr-link-dialog.component.html',
    styleUrls: ['./qr-link-dialog.component.less']
})
export class QrLinkDialogComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: IQrLinkDialogData) {
    }

    ngOnInit(): void {
    }

}
