import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    templateUrl: './markdown-preview-dialog.component.html',
    styleUrls: ['./markdown-preview-dialog.component.less']
})
export class MarkdownPreviewDialogComponent implements OnInit {

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: string
    ) {
    }

    ngOnInit(): void {
    }

}
