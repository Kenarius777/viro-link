import { IShortLink } from '../interfaces';
import { LinkType } from '../../shared/shared.constants';

export class ShortLinkModel implements IShortLink{
    code: string;
    shortLink: string;
    deletionKey: string;
    preview: LinkDataPreview | TextDataPreview;

    constructor(data: IShortLink) {
        this.code = data.code;
        this.shortLink = data.shortLink;
        this.deletionKey = data.deletionKey;
        if ('link' in data.preview) {
            this.preview = new LinkDataPreview(data.preview.link);
        } else {
            this.preview = new TextDataPreview(data.preview.text);
        }
    }
}

export class LinkDataPreview {
    type: LinkType = LinkType.LINK;
    link: string;

    constructor(link: string) {
        this.link = link;
    }
}

export class TextDataPreview {
    type: LinkType = LinkType.TEXT;
    text: string;

    constructor(text: string) {
        this.text = text;
    }
}
