import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { Subject } from 'rxjs';
import { FORM_ERROR_MESSAGES } from '../../../di/form-errors/form-error-messages.token';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { ShortLinkModel } from '../../../models';
import { IShortLinkInfo } from '../../../interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ShortenLinkResultDialogComponent } from '../../../dialogs/shorten-link-result-dialog/shorten-link-result-dialog.component';
import { filter, map, takeUntil } from 'rxjs/operators';
import { NotifyService } from '../../../../shared/services';
import { environment } from '../../../../../environments/environment';
import { IFormErrorMessages } from '../../../di/form-errors/interfaces/form-error-messages.interface';
import { StringUtils } from '../../../../shared/utils';
import { Actions, ofType } from '@ngrx/effects';
import { SHORT_LINK_ACTIONS, ShortLinkActions } from '../../../store/short-link.actions';

@Component({
    template: '',
})
export abstract class BaseShortenLinkFormComponent implements OnInit, OnDestroy {

    readonly shortLinkUrl: string = environment.apiUrl;
    form!: FormGroup;
    @ViewChild(FormGroupDirective) formGroupDirective!: FormGroupDirective;
    shortCodeFieldVisibility: boolean = false;

    private requestId: string | null = null;
    private get formSubmitDisabled(): boolean {
        return this.requestId !== null;
    }
    private readonly isMobileSource = this.breakpointObserver.observe('(max-width: 575px)');
    private isMobile: boolean = false;
    protected readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        protected breakpointObserver: BreakpointObserver,
        protected router: Router,
        protected route: ActivatedRoute,
        protected matDialog: MatDialog,
        protected fb: FormBuilder,
        protected notifyService: NotifyService,
        protected actions$: Actions<ShortLinkActions>,
        @Inject(FORM_ERROR_MESSAGES) protected errorMessageList: IFormErrorMessages
    ) {
    }

    ngOnInit(): void {
        this.form = this.returnEmptyForm();

        this.isMobileSource.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe((result: BreakpointState) => {
            this.isMobile = result.matches;
        });

        this.actions$.pipe(
            ofType(SHORT_LINK_ACTIONS.SAVE_DATA_SUCCESS),
            filter(() => this.requestId !== null),
            filter(action => action.requestId === this.requestId),
            map(action => action.data),
            takeUntil(this.unsubscriber$)
        ).subscribe(data => {
            this.onShortenLinkComplete();
            this.onShortenLinkSuccess(data);
            this.resetForm(this.getEmptyFormData());
        });

        this.actions$.pipe(
            ofType(SHORT_LINK_ACTIONS.SAVE_DATA_ERROR),
            filter(() => this.requestId !== null),
            filter(action => action.requestId === this.requestId),
            map(action => action.error),
            takeUntil(this.unsubscriber$)
        ).subscribe(error => {
            this.onShortenLinkComplete();
            this.onShortenLinkError(error);
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    onShortLinkDisplayClick(): void {
        this.shortCodeFieldVisibility = true;
    }

    submitForm(): void {
        if (this.form.valid && !this.formSubmitDisabled) {
            this.requestId = StringUtils.uuidv4();
            this.dispatchSaveAction(this.requestId);
        }
    }

    getErrorMessage(controlName: string): string | null {
        const control = this.form.get(controlName)!;
        const errorTypes = Object.keys(control.errors || {});
        if (errorTypes.length > 0) {
            const errorType = errorTypes.find(() => true)!;
            const errorMessageGetter = this.errorMessageList[errorType];
            if (errorMessageGetter) {
                return errorMessageGetter(control.errors![errorType]);
            }
        }
        return null;
    }

    protected abstract dispatchSaveAction(requestId: string): void;

    protected abstract getEmptyFormData(): any;

    protected abstract returnEmptyForm(): FormGroup;

    protected resetForm(defaultFormValues: any): void {
        this.formGroupDirective.resetForm();
        this.form.patchValue(defaultFormValues);
    }

    private onShortenLinkComplete(): void {
        this.requestId = null;
    }

    private onShortenLinkSuccess(shortLink: ShortLinkModel): void {
        if (this.isMobile) {
            this.router.navigate(['../result', shortLink.code], { relativeTo: this.route });
        } else {
            const shortLinkInfo: IShortLinkInfo = {
                code: shortLink.code,
                link: shortLink.shortLink
            };
            this.matDialog.open(ShortenLinkResultDialogComponent, {
                data: shortLinkInfo,
                closeOnNavigation: true
            });
        }
    }

    private onShortenLinkError(error: Error): void {
        this.notifyService.showError(error);
    }
}
