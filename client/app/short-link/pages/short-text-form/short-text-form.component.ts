import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShortLinkService } from '../../services';
import { takeUntil } from 'rxjs/operators';
import { CenteredPageSizeService } from '../../../ui/services';
import { IInlineSelectorItem } from '../../../ui/interfaces';
import { TEXT_FORM_ERROR_MESSAGES } from '../../short-link.constants';
import { BaseShortenLinkFormComponent } from '../base/base-shorten-link-form/base-shorten-link-form.component';
import { FORM_ERROR_MESSAGES } from '../../di/form-errors/form-error-messages.token';
import { shortCodeValidator } from '../../short-code.validation';
import { ITextSaveFormData } from '../../interfaces';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MarkdownPreviewDialogComponent } from '../../dialogs/markdown-preview-dialog/markdown-preview-dialog.component';
import { MarkdownService, NotifyService } from '../../../shared/services';
import { SharedValidators } from '../../../shared/utils';
import { IFormErrorMessages } from '../../di/form-errors/interfaces/form-error-messages.interface';
import { Actions } from '@ngrx/effects';
import { ShortLinkActions, shortLinkSaveTextData } from '../../store/short-link.actions';
import { IShortLinkState } from '../../store/short-link.state';
import { Store } from '@ngrx/store';
import { ShortenTextType } from '../../../shared/shared.constants';
import { CENTERED_PAGE_SIZE_WIDTH_LIST, CenteredPageSize } from '../../../ui/ui.constants';

@Component({
    selector: 'app-short-text-form',
    templateUrl: './short-text-form.component.html',
    styleUrls: ['./short-text-form.component.less'],
    providers: [{
        provide: FORM_ERROR_MESSAGES,
        useValue: TEXT_FORM_ERROR_MESSAGES
    }]
})
export class ShortTextFormComponent extends BaseShortenLinkFormComponent implements OnInit, OnDestroy {

    isMarkdownTextTypeSelected: boolean = false;

    pageSizeList: Array<IInlineSelectorItem> = [
        { key: CenteredPageSize.SMALL, value: 'small' },
        { key: CenteredPageSize.MEDIUM, value: 'medium' },
        { key: CenteredPageSize.LARGE, value: 'large' },
    ];

    textTypeList: Array<IInlineSelectorItem> = [
        { key: ShortenTextType.SIMPLE_TEXT, value: 'simple text' },
        { key: ShortenTextType.MARKDOWN, value: 'markdown' },
    ];

    constructor(
        private markdownService: MarkdownService,
        private pageSizeService: CenteredPageSizeService,
        private shortLinkService: ShortLinkService,
        private store: Store<IShortLinkState>,
        breakpointObserver: BreakpointObserver,
        router: Router,
        route: ActivatedRoute,
        matDialog: MatDialog,
        fb: FormBuilder,
        notifyService: NotifyService,
        actions$: Actions<ShortLinkActions>,
        @Inject(FORM_ERROR_MESSAGES) errorMessageList: IFormErrorMessages
    ) {
        super(breakpointObserver, router, route, matDialog, fb, notifyService, actions$, errorMessageList);
    }

    ngOnInit(): void {
        super.ngOnInit();
        this.initFormListeners();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    markdownPreview(): void {
        const width = CENTERED_PAGE_SIZE_WIDTH_LIST[this.form.get('pageSize')!.value] + 'px';
        const markdownPreview = this.markdownService.convert(this.form.get('text')!.value);
        this.matDialog.open(MarkdownPreviewDialogComponent, {
            data: markdownPreview,
            width: width
        });
    }

    protected returnEmptyForm(): FormGroup {
        return this.fb.group({
            code: [null, [Validators.minLength(3), Validators.maxLength(64), shortCodeValidator]],
            text: ['', [SharedValidators.requiredWithoutSpaces, Validators.maxLength(16000)]],
            textType: [ShortenTextType.SIMPLE_TEXT, [Validators.required]],
            pageSize: [CenteredPageSize.SMALL, [Validators.required]]
        });
    }

    protected dispatchSaveAction(requestId: string): void {
        const formData = this.form.getRawValue() as ITextSaveFormData;
        this.store.dispatch(shortLinkSaveTextData({ requestId, data: formData }));
    }

    protected getEmptyFormData(): ITextSaveFormData {
        return {
            code: null,
            text: '',
            textType: ShortenTextType.SIMPLE_TEXT,
            pageSize: CenteredPageSize.SMALL
        };
    }

    private initFormListeners(): void {
        this.form.get('pageSize')!.valueChanges.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(nextSize => {
            this.setSize(nextSize);
        });
        this.form.get('textType')!.valueChanges.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(nextType => {
            this.isMarkdownTextTypeSelected = nextType === ShortenTextType.MARKDOWN;
        });
    }

    private setSize(size: CenteredPageSize) {
        this.pageSizeService.setPageSize(size);
    }

}
