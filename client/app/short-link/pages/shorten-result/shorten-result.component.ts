import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { IShortLinkInfo } from '../../interfaces';

@Component({
    selector: 'app-shorten-result',
    templateUrl: './shorten-result.component.html',
    styleUrls: ['./shorten-result.component.less']
})
export class ShortenResultComponent implements OnInit, OnDestroy {

    linkInfoSource!: Observable<IShortLinkInfo>;

    private unsubscriber$: Subject<void> = new Subject();

    constructor(
        private route: ActivatedRoute,
    ) {
    }

    ngOnInit(): void {
        this.initLinkInfoSource();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private initLinkInfoSource(): void {
        this.linkInfoSource = this.route.data.pipe(
            map(d => d.linkInfo),
            filter(d => !!d),
            distinctUntilChanged(),
        );
    }
}
