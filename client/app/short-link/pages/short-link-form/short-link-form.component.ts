import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ShortLinkService } from '../../services';
import { SharedValidators } from '../../../shared/utils';
import { LINK_FORM_ERROR_MESSAGES } from '../../short-link.constants';
import { shortCodeValidator } from '../../short-code.validation';
import { ILinkSaveFormData } from '../../interfaces';
import { BaseShortenLinkFormComponent } from '../base/base-shorten-link-form/base-shorten-link-form.component';
import { FORM_ERROR_MESSAGES } from '../../di/form-errors/form-error-messages.token';
import { BreakpointObserver } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { NotifyService } from '../../../shared/services';
import { IFormErrorMessages } from '../../di/form-errors/interfaces/form-error-messages.interface';
import { IShortLinkState } from '../../store/short-link.state';
import { Store } from '@ngrx/store';
import { ShortLinkActions, shortLinkSaveLinkData } from '../../store/short-link.actions';
import { Actions } from '@ngrx/effects';

@Component({
    selector: 'app-short-link-form',
    templateUrl: './short-link-form.component.html',
    styleUrls: ['./short-link-form.component.less'],
    providers: [{
        provide: FORM_ERROR_MESSAGES,
        useValue: LINK_FORM_ERROR_MESSAGES
    }]
})
export class ShortLinkFormComponent extends BaseShortenLinkFormComponent implements OnInit, OnDestroy {

    constructor(
        private shortLinkService: ShortLinkService,
        private store: Store<IShortLinkState>,
        breakpointObserver: BreakpointObserver,
        router: Router,
        route: ActivatedRoute,
        matDialog: MatDialog,
        fb: FormBuilder,
        notifyService: NotifyService,
        actions$: Actions<ShortLinkActions>,
        @Inject(FORM_ERROR_MESSAGES) errorMessageList: IFormErrorMessages
    ) {
        super(breakpointObserver, router, route, matDialog, fb, notifyService, actions$, errorMessageList);
    }

    ngOnInit(): void {
        super.ngOnInit();
    }

    ngOnDestroy(): void {
        super.ngOnDestroy();
    }

    protected returnEmptyForm(): FormGroup {
        return this.fb.group({
            code: [null, [Validators.minLength(3), Validators.maxLength(64), shortCodeValidator]],
            link: ['', [Validators.required, SharedValidators.url, Validators.maxLength(2048)]]
        });
    }

    protected getEmptyFormData(): ILinkSaveFormData {
        return {
            code: null,
            link: ''
        };
    }

    protected dispatchSaveAction(requestId: string): void {
        const formData = this.form.getRawValue() as ILinkSaveFormData;
        this.store.dispatch(shortLinkSaveLinkData({ requestId, data: formData }));
    }

}
