import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-short-link-page-layout',
    templateUrl: './short-link-page-layout.component.html',
    styleUrls: ['./short-link-page-layout.component.less']
})
export class ShortLinkPageLayoutComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
    }

}
