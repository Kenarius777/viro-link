import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { IShortLinkInfo } from '../interfaces';
import { environment } from '../../../environments/environment';

@Injectable()
export class ShortLinkResultResolver implements Resolve<IShortLinkInfo> {
    constructor() {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): IShortLinkInfo {
        const linkCode: string = route.params.code;
        return {
            code: linkCode,
            link: `${environment.apiUrl}/${linkCode}`
        };
    }
}
