import { Component, Input, OnInit } from '@angular/core';
import { IShortLinkInfo } from '../../interfaces';
import { CopyService } from '../../../shared/services';

@Component({
    selector: 'app-shorten-link-info',
    templateUrl: './shorten-link-info.component.html',
    styleUrls: ['./shorten-link-info.component.less']
})
export class ShortenLinkInfoComponent implements OnInit {

    @Input() linkInfo!: IShortLinkInfo;

    copySuccessful: boolean = false;

    constructor(
        private copyService: CopyService
    ) {
    }

    ngOnInit(): void {
        this.copyLink();
    }

    copyLink(): void {
        this.copySuccessful = this.copyService.copyToClipboard(this.linkInfo.link);
    }
}
