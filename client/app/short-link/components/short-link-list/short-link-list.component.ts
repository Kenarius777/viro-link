import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ShortLinkModel } from '../../models';
import { animate, style, transition, trigger } from '@angular/animations';
import { IShortLinkState } from '../../store/short-link.state';
import { select, Store } from '@ngrx/store';
import { shortLinkFetchList } from '../../store/short-link.actions';
import { shortLinkListSelector } from '../../store/short-link.selectors';

@Component({
    selector: 'app-short-link-list',
    templateUrl: './short-link-list.component.html',
    styleUrls: ['./short-link-list.component.less'],
    animations: [
        trigger('disappear', [
            transition(':leave', [
                animate('1s', style({ opacity: 0 }))
            ])
        ]),
        trigger('deletion', [
            transition(':leave', [
                style({
                    overflow: 'hidden',
                    backgroundColor: 'rgba(255, 0, 0, 0.36)'
                }),
                animate('0.8s 0.2s ease', style({
                    height: 0,
                }))
            ])
        ])
    ]
})
export class ShortLinkListComponent implements OnInit {

    shortLinkListSource!: Observable<ReadonlyArray<ShortLinkModel>>;

    constructor(
        private store: Store<IShortLinkState>,
    ) {
    }

    ngOnInit(): void {
        this.store.dispatch(shortLinkFetchList());
        this.shortLinkListSource = this.store.pipe(select(shortLinkListSelector));
    }

}
