import { ChangeDetectionStrategy, Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ShortLinkModel } from '../../../models';
import { LinkType } from '../../../../shared/shared.constants';
import { MatDialog } from '@angular/material/dialog';
import { QrLinkDialogComponent } from '../../../dialogs/qr-link-dialog/qr-link-dialog.component';
import { CopyService, NotifyService } from '../../../../shared/services';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IShortLinkState } from '../../../store/short-link.state';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { SHORT_LINK_ACTIONS, ShortLinkActions, shortLinkDeleteItem } from '../../../store/short-link.actions';
import { QRCodeComponent } from '../../../../ui/components/qr-code/qr-code.component';
import { DEFAULT_QR_LEFT_POSITION, MIN_X_QR_OFFSET } from './short-link-list-item.constants';

@Component({
    selector: 'app-short-link-list-item',
    templateUrl: './short-link-list-item.component.html',
    styleUrls: ['./short-link-list-item.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShortLinkListItemComponent implements OnInit, OnDestroy {

    @Input() shortLink!: ShortLinkModel;

    @ViewChild(QRCodeComponent, { read: ElementRef, static: true })
    qrCodeComponent!: ElementRef<HTMLDivElement>;

    qrLeftPosition: number = DEFAULT_QR_LEFT_POSITION;
    readonly linkTypes: typeof LinkType = LinkType;

    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        private matDialog: MatDialog,
        private notifyService: NotifyService,
        private copyService: CopyService,
        private store: Store<IShortLinkState>,
        private actions$: Actions<ShortLinkActions>
    ) {
    }

    ngOnInit(): void {
        this.actions$.pipe(
            ofType(SHORT_LINK_ACTIONS.DELETE_ITEM_ERROR),
            filter(action => action.code === this.shortLink.code),
            takeUntil(this.unsubscriber$)
        ).subscribe(action => {
            this.notifyService.showError(action.error);
        });
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    copyLink(): void {
        const result = this.copyService.copyToClipboard(this.shortLink.shortLink);
        if (result) {
            this.notifyService.show('copied');
        }
    }

    showQrDialog(): void {
        this.matDialog.open(QrLinkDialogComponent, {
            data: { link: this.shortLink.shortLink }
        });
    }

    deleteLink(): void {
        this.store.dispatch(shortLinkDeleteItem({ code: this.shortLink.code, key: this.shortLink.deletionKey }));
    }

    checkQrPosition(): void {
        const domRect = this.qrCodeComponent.nativeElement.getBoundingClientRect();
        let newLeftPosition = this.qrLeftPosition;
        newLeftPosition = newLeftPosition - (domRect.x - MIN_X_QR_OFFSET);
        if (domRect.x >= MIN_X_QR_OFFSET) {
            newLeftPosition = Math.max(newLeftPosition, DEFAULT_QR_LEFT_POSITION);
        }
        this.qrLeftPosition = newLeftPosition;
    }
}
