import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShortLinkPageLayoutComponent } from './layouts/short-link-page-layout/short-link-page-layout.component';
import { ShortLinkFormComponent } from './pages/short-link-form/short-link-form.component';
import { ShortTextFormComponent } from './pages/short-text-form/short-text-form.component';
import { ShortenResultComponent } from './pages/shorten-result/shorten-result.component';
import { ShortLinkResultResolver } from './resolvers';
import { ShareLinkGuard, ShortLinkExistGuard } from './guards';

const routes: Routes = [
    {
        path: '',
        component: ShortLinkPageLayoutComponent,
        children: [
            {
                path: 'link',
                component: ShortLinkFormComponent
            },
            {
                path: 'text',
                component: ShortTextFormComponent
            }
        ]
    },
    {
        path: 'share',
        canActivate: [ ShareLinkGuard ]
    },
    {
        path: 'result/:code',
        component: ShortenResultComponent,
        canActivate: [ ShortLinkExistGuard ],
        resolve: {
            linkInfo: ShortLinkResultResolver
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShortLinkRoutingModule {
}
