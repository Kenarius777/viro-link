import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiModule } from '../api/api.module';
import { ShortLinkRoutingModule } from './short-link-routing.module';
import { ShortLinkFormComponent } from './pages/short-link-form/short-link-form.component';
import { ShortTextFormComponent } from './pages/short-text-form/short-text-form.component';
import { ShortLinkPageLayoutComponent } from './layouts/short-link-page-layout/short-link-page-layout.component';
import { ShortLinkListComponent } from './components/short-link-list/short-link-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShareTargetDataService, ShortLinkService, ShortLinkStorageService } from './services';
import {
    DeletionKeyToRequestMapper,
    SavedShortLinkResponseToModelMapper,
    SaveLinkFormDataToRequestMapper,
    SaveTextFormDataToRequestMapper
} from './mappers';
import { UIModule } from '../ui/ui.module';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ShortLinkListItemComponent } from './components/short-link-list/short-link-list-item/short-link-list-item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { QrLinkDialogComponent } from './dialogs/qr-link-dialog/qr-link-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ShortenResultComponent } from './pages/shorten-result/shorten-result.component';
import { ShortenLinkInfoComponent } from './components/shorten-link-info/shorten-link-info.component';
import { ShareLinkGuard, ShortLinkExistGuard } from './guards';
import { ShortenLinkResultDialogComponent } from './dialogs/shorten-link-result-dialog/shorten-link-result-dialog.component';
import { MarkdownPreviewDialogComponent } from './dialogs/markdown-preview-dialog/markdown-preview-dialog.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { shortLinkReducer } from './store/short-link.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ShortLinkEffects } from './store/short-link.effects';
import { SHORT_LINK_FEATURE_KEY } from './store/short-link.state';

@NgModule({
    declarations: [
        ShortLinkPageLayoutComponent,
        ShortLinkFormComponent,
        ShortTextFormComponent,
        ShortLinkListComponent,
        ShortLinkListItemComponent,
        QrLinkDialogComponent,
        ShortenResultComponent,
        ShortenLinkInfoComponent,
        MarkdownPreviewDialogComponent,
        ShortenLinkResultDialogComponent,
    ],
    imports: [
        ApiModule,
        ShortLinkRoutingModule,
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        UIModule,
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        FontAwesomeModule,
        MatDialogModule,
        SharedModule,
        StoreModule.forFeature(SHORT_LINK_FEATURE_KEY, shortLinkReducer),
        EffectsModule.forFeature([
            ShortLinkEffects
        ])
    ],
    providers: [
        ShortLinkService,
        ShortLinkStorageService,
        SaveLinkFormDataToRequestMapper,
        SaveTextFormDataToRequestMapper,
        SavedShortLinkResponseToModelMapper,
        DeletionKeyToRequestMapper,
        ShortLinkExistGuard,
        ShareLinkGuard,
        ShareTargetDataService,
    ],
})
export class ShortLinkModule {
}
