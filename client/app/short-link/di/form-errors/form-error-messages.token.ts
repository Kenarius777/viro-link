import { InjectionToken } from '@angular/core';
import { IFormErrorMessages } from './interfaces/form-error-messages.interface';

export const FORM_ERROR_MESSAGES = new InjectionToken<IFormErrorMessages>('form error message getters');
