export interface IFormErrorMessages { [key: string]: (errorData: any) => string; }
