import { IFormErrorMessages } from './di/form-errors/interfaces/form-error-messages.interface';

export const LINK_FORM_ERROR_MESSAGES: IFormErrorMessages = {
    minlength: ({ requiredLength }) => `minimal length: ${requiredLength}`,
    maxlength: ({ requiredLength }) => `maximal length: ${requiredLength}`,
    shortcode: () => 'only letters and numbers allowed',
    required: () => 'required',
    url: () => 'url incorrect'
};

export const TEXT_FORM_ERROR_MESSAGES: IFormErrorMessages = {
    minlength: ({ requiredLength }) => `minimal length: ${requiredLength}`,
    maxlength: ({ requiredLength }) => `maximal length: ${requiredLength}`,
    shortcode: () => 'only letters and numbers allowed',
    required: () => 'required'
};

