import { Injectable } from '@angular/core';
import { ApiService } from '../../api/services';
import { BehaviorSubject, Observable } from 'rxjs';
import { ILinkSaveFormData, ITextSaveFormData } from '../interfaces';
import { ShortLinkModel } from '../models';
import {
    DeletionKeyToRequestMapper,
    SavedShortLinkResponseToModelMapper,
    SaveLinkFormDataToRequestMapper,
    SaveTextFormDataToRequestMapper
} from '../mappers';
import { map, take, tap } from 'rxjs/operators';
import { ShortLinkStorageService } from './short-link-storage.service';

@Injectable()
export class ShortLinkService {

    constructor(
        private apiService: ApiService,
        private shortLinkStorageService: ShortLinkStorageService,
        private saveLinkFormDataToRequestMapper: SaveLinkFormDataToRequestMapper,
        private saveTextFormDataToRequestMapper: SaveTextFormDataToRequestMapper,
        private savedShortLinkResponseToModelMapper: SavedShortLinkResponseToModelMapper,
        private deletionKeyToRequestMapper: DeletionKeyToRequestMapper,
    ) {
    }

    fetchLinkList(): Observable<Array<ShortLinkModel>> {
        return this.shortLinkStorageService.shortLinkListSource.pipe(
            take(1)
        );
    }

    isLinkExist(code: string): Observable<boolean> {
        return this.apiService.getLinkContent(code).pipe(
            map(i => true)
        );
    }

    saveLinkData(formData: ILinkSaveFormData): Observable<ShortLinkModel> {
        const request = this.saveLinkFormDataToRequestMapper.map(formData);
        return this.apiService.saveLinkData(request).pipe(
            map(response => this.savedShortLinkResponseToModelMapper.map(response))
        );
    }

    saveTextData(formData: ITextSaveFormData): Observable<ShortLinkModel> {
        const request = this.saveTextFormDataToRequestMapper.map(formData);
        return this.apiService.saveTextData(request).pipe(
            map(response => this.savedShortLinkResponseToModelMapper.map(response))
        );
    }

    deleteShortLink(code: string, key: string): Observable<boolean> {
        const request = this.deletionKeyToRequestMapper.map(key);
        return this.apiService.deleteShortLink(code, request).pipe(
            tap(() => {
                this.shortLinkStorageService.removeLink(code);
            })
        );
    }
}
