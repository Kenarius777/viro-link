import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShortLinkModel } from '../models';
import { isValidHttpUrl } from '../../shared/utils';
import { ShortLinkService } from './short-link.service';
import { ShortenTextType } from '../../shared/shared.constants';
import { CenteredPageSize } from '../../ui/ui.constants';

@Injectable()
export class ShareTargetDataService {
    constructor(
        private shortLinkService: ShortLinkService,
    ) {
    }

    shareData(title: string | undefined, text: string | undefined, url: string | undefined): Observable<ShortLinkModel> {
        const sharingUrl = url || title || text;
        if (!sharingUrl) {
            throw new Error('share data empty');
        }
        if (isValidHttpUrl(sharingUrl)) {
            return this.shortLinkService.saveLinkData({code: null, link: sharingUrl});
        }

        const textType = title ? ShortenTextType.MARKDOWN : ShortenTextType.SIMPLE_TEXT;
        const savedText = title ? `**${title}**\n-------\n${text}` : text;
        return this.shortLinkService.saveTextData(
            {
                code: null,
                pageSize: CenteredPageSize.SMALL,
                textType: textType,
                text: savedText!
            }
        );
    }
}
