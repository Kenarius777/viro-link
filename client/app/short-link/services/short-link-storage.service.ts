import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ShortLinkModel } from '../models';
import { ILocalStorage } from '../../di/local-storage/interfaces';
import { LOCAL_STORAGE } from '../../di/local-storage/local-storage.token';

@Injectable()
export class ShortLinkStorageService {

    private shortLinkList$: BehaviorSubject<Array<ShortLinkModel>> = new BehaviorSubject([]);
    shortLinkListSource: Observable<Array<ShortLinkModel>> = this.shortLinkList$.asObservable();

    constructor(
        @Inject(LOCAL_STORAGE) private localStorage: ILocalStorage,
    ) {
        const savedShortLinks = this.localStorage.getItem('short_link_list');
        if (savedShortLinks) {
            try {
                this.shortLinkList$.next(JSON.parse(savedShortLinks).map(i => new ShortLinkModel(i)));
            } catch (_) {
            }
        }
    }

    removeLink(code: string): void {
        const nextList = this.shortLinkList$.value.filter(item => item.code !== code);
        this.localStorage.setItem('short_link_list', JSON.stringify(nextList));
        this.shortLinkList$.next(nextList);
    }

    addLink(shortLink: ShortLinkModel): void {
        const prevList = this.shortLinkList$.value;
        const nextList = [shortLink, ...prevList];
        this.localStorage.setItem('short_link_list', JSON.stringify(nextList));
        this.shortLinkList$.next(nextList);
    }
}
