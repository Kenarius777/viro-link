import { Injectable } from '@angular/core';
import { ILinkSaveFormData } from '../interfaces';
import { ISaveLinkDataRequest } from '../../api/interfaces';

@Injectable()
export class SaveLinkFormDataToRequestMapper {

    constructor() {
    }

    map(data: ILinkSaveFormData): ISaveLinkDataRequest {
        return {
            shortCode: data.code,
            link: data.link
        };
    }
}
