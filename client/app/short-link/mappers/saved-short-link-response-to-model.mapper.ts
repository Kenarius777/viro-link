import { Injectable } from '@angular/core';
import { ISavedShortLinkResponse } from '../../api/interfaces';
import { ShortLinkModel } from '../models';

@Injectable()
export class SavedShortLinkResponseToModelMapper {

    constructor() {
    }

    map(data: ISavedShortLinkResponse): ShortLinkModel {
        return new ShortLinkModel({
            code: data.code,
            shortLink: data.shortLink,
            deletionKey: data.deletionKey,
            preview: data.preview
        });
    }
}
