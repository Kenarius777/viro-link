import { Injectable } from '@angular/core';
import { IDeleteShortLinkRequest } from '../../api/interfaces';

@Injectable()
export class DeletionKeyToRequestMapper {

    constructor() {
    }

    map(key: string): IDeleteShortLinkRequest {
        return {
            key: key
        };
    }
}
