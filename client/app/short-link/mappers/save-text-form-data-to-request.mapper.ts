import { Injectable } from '@angular/core';
import { ITextSaveFormData } from '../interfaces';
import { ISaveTextDataRequest } from '../../api/interfaces';

@Injectable()
export class SaveTextFormDataToRequestMapper {

    constructor() {
    }

    map(data: ITextSaveFormData): ISaveTextDataRequest {
        return {
            shortCode: data.code,
            text: data.text,
            pageSize: data.pageSize,
            textType: data.textType,
        };
    }


}
