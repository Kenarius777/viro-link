import { ShortLinkModel } from '../models';

export const SHORT_LINK_FEATURE_KEY = 'shortLink';

export interface IShortLinkState {
    list: ReadonlyArray<ShortLinkModel>;
    listError: Error | null;
}

export const SHORT_LINK_DEFAULT_STATE: IShortLinkState = {
    list: [],
    listError: null
};
