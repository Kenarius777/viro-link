import { Action, createReducer, on } from '@ngrx/store';
import { IShortLinkState, SHORT_LINK_DEFAULT_STATE } from './short-link.state';
import {
    shortLinkDeleteItemSuccess,
    shortLinkFetchListError,
    shortLinkFetchListSuccess,
    shortLinkSaveDataSuccess
} from './short-link.actions';

const reducer = createReducer(
    SHORT_LINK_DEFAULT_STATE,
    on(shortLinkFetchListSuccess, (state, { list }) => ({ ...state, list: list, listError: null })),
    on(shortLinkFetchListError, (state, { error }) => ({ ...state, list: [], listError: error })),
    on(shortLinkDeleteItemSuccess, (state, { code }) =>
        ({ ...state, list: state.list.filter(i => i.code !== code) })
    ),
    on(shortLinkSaveDataSuccess, (state, { data }) =>
        ({ ...state, list: [data, ...state.list] })
    )
);

export function shortLinkReducer(state: IShortLinkState | undefined, action: Action): IShortLinkState {
    return reducer(state, action);
}
