import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IShortLinkState, SHORT_LINK_FEATURE_KEY } from './short-link.state';

export const shortLinkStateSelector = createFeatureSelector<IShortLinkState>(SHORT_LINK_FEATURE_KEY);

export const shortLinkListSelector = createSelector(
    shortLinkStateSelector,
    (state: IShortLinkState) => state.list
);
