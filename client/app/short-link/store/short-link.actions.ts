import { createAction, props, union } from '@ngrx/store';
import { ShortLinkModel } from '../models';
import { ILinkSaveFormData, ITextSaveFormData } from '../interfaces';

export const enum SHORT_LINK_ACTIONS {
    FETCH_LIST = '[SHORT LINK] fetch list',
    FETCH_LIST_SUCCESS = '[SHORT LINK] fetch list success',
    FETCH_LIST_ERROR = '[SHORT LINK] fetch list error',
    DELETE_ITEM = '[SHORT LINK] delete item',
    DELETE_ITEM_SUCCESS = '[SHORT LINK] delete item success',
    DELETE_ITEM_ERROR = '[SHORT LINK] delete item error',
    SAVE_LINK_DATA = '[SHORT LINK] save link data',
    SAVE_TEXT_DATA = '[SHORT LINK] save text data',
    SAVE_DATA_SUCCESS = '[SHORT LINK] save data success',
    SAVE_DATA_ERROR = '[SHORT LINK] save data error',
}

export const shortLinkFetchList = createAction(
    SHORT_LINK_ACTIONS.FETCH_LIST,
);

export const shortLinkFetchListSuccess = createAction(
    SHORT_LINK_ACTIONS.FETCH_LIST_SUCCESS,
    props<{ list: ReadonlyArray<ShortLinkModel> }>()
);

export const shortLinkFetchListError = createAction(
    SHORT_LINK_ACTIONS.FETCH_LIST_ERROR,
    props<{ error: Error | null }>()
);

export const shortLinkDeleteItem = createAction(
    SHORT_LINK_ACTIONS.DELETE_ITEM,
    props<{ code: string, key: string }>()
);

export const shortLinkDeleteItemSuccess = createAction(
    SHORT_LINK_ACTIONS.DELETE_ITEM_SUCCESS,
    props<{ code: string }>()
);

export const shortLinkDeleteItemError = createAction(
    SHORT_LINK_ACTIONS.DELETE_ITEM_ERROR,
    props<{ code: string, error: Error }>()
);

export const shortLinkSaveLinkData = createAction(
    SHORT_LINK_ACTIONS.SAVE_LINK_DATA,
    props<{ requestId: string, data: ILinkSaveFormData }>()
);

export const shortLinkSaveTextData = createAction(
    SHORT_LINK_ACTIONS.SAVE_TEXT_DATA,
    props<{ requestId: string, data: ITextSaveFormData }>()
);

export const shortLinkSaveDataSuccess = createAction(
    SHORT_LINK_ACTIONS.SAVE_DATA_SUCCESS,
    props<{ requestId: string, data: ShortLinkModel }>()
);

export const shortLinkSaveDataError = createAction(
    SHORT_LINK_ACTIONS.SAVE_DATA_ERROR,
    props<{ requestId: string, error: Error }>()
);

const shortLinkActionsUnion = union({
    shortLinkFetchList,
    shortLinkFetchListSuccess,
    shortLinkFetchListError,
    shortLinkDeleteItem,
    shortLinkDeleteItemSuccess,
    shortLinkDeleteItemError,
    shortLinkSaveLinkData,
    shortLinkSaveTextData,
    shortLinkSaveDataSuccess,
    shortLinkSaveDataError,
});

export type ShortLinkActions = typeof shortLinkActionsUnion;
