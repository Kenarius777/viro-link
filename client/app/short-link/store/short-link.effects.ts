import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {
    SHORT_LINK_ACTIONS,
    ShortLinkActions,
    shortLinkDeleteItemError,
    shortLinkDeleteItemSuccess,
    shortLinkFetchListError,
    shortLinkFetchListSuccess, shortLinkSaveDataError, shortLinkSaveDataSuccess
} from './short-link.actions';
import { catchError, exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { ShortLinkService, ShortLinkStorageService } from '../services';

@Injectable()
export class ShortLinkEffects {

    shortLinkList = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.FETCH_LIST),
        switchMap(() => this.shortLinkService.fetchLinkList().pipe(
            map(shortLinkList => shortLinkFetchListSuccess({ list: shortLinkList})),
            catchError(error => of(shortLinkFetchListError({ error: error})))
        ))
    ));

    shortLinkDeleteItem = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.DELETE_ITEM),
        exhaustMap(action => this.shortLinkService.deleteShortLink(action.code, action.key).pipe(
            map(() => shortLinkDeleteItemSuccess({ code: action.code })),
            catchError(error => of(shortLinkDeleteItemError({ code: action.code, error: error })))
        ))
    ));

    shortLinkDeleteItemSuccess = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.DELETE_ITEM_SUCCESS),
        tap(action => this.shortLinkStorageService.removeLink(action.code))
    ), { dispatch: false});

    shortLinkSaveLinkData = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.SAVE_LINK_DATA),
        switchMap(action => this.shortLinkService.saveLinkData(action.data).pipe(
            map(savedShortLink =>
                shortLinkSaveDataSuccess({ requestId: action.requestId, data: savedShortLink })
            ),
            catchError(error => of(shortLinkSaveDataError({ requestId: action.requestId, error: error })))
        ))
    ));

    shortLinkSaveTextData = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.SAVE_TEXT_DATA),
        switchMap(action => this.shortLinkService.saveTextData(action.data).pipe(
            map(savedShortLink =>
                shortLinkSaveDataSuccess({ requestId: action.requestId, data: savedShortLink })
            ),
            catchError(error => of(shortLinkSaveDataError({ requestId: action.requestId, error: error })))
        ))
    ));

    shortLinkSaveDataSuccess = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_ACTIONS.SAVE_DATA_SUCCESS),
        tap(action => this.shortLinkStorageService.addLink(action.data))
    ), { dispatch: false });

    constructor(
        private actions$: Actions<ShortLinkActions>,
        private shortLinkService: ShortLinkService,
        private shortLinkStorageService: ShortLinkStorageService,
    ) {
    }
}
