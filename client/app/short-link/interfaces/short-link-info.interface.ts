export interface IShortLinkInfo {
    code: string;
    link: string;
}
