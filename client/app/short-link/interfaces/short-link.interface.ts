export interface IShortLink {
    code: string;
    shortLink: string;
    deletionKey: string;
    preview: ILinkDataPreview | ITextDataPreview;
}

export interface ILinkDataPreview {
    link: string;
}

export interface ITextDataPreview {
    text: string;
}
