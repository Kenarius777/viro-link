export interface ILinkSaveFormData {
    code: string | null;
    link: string;
}
