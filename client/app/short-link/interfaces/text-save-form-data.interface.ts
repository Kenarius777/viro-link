export interface ITextSaveFormData {
    code: string | null;
    text: string;
    pageSize: string;
    textType: string;
}
