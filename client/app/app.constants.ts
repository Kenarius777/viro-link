import { faChevronLeft, faQrcode, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { faClipboard } from '@fortawesome/free-regular-svg-icons';

export const APP_ICONS = [
    faQrcode, faClipboard, faTrashAlt, faChevronLeft
];
