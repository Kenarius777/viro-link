import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ShortLinkContentModel } from '../models';
import { IShortLinkContentState } from '../store/short-link-content.state';
import { select, Store } from '@ngrx/store';
import { shortLinkContentSelector } from '../store/short-link-content.selectors';
import { filter, map, take } from 'rxjs/operators';

@Injectable()
export class ShortLinkContentResolver implements Resolve<ShortLinkContentModel> {
    constructor(
        private store: Store<IShortLinkContentState>
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShortLinkContentModel> {
        return this.store.pipe(select(shortLinkContentSelector)).pipe(
            filter(content => !!content),
            map(content => content as ShortLinkContentModel),
            take(1)
        );
    }
}
