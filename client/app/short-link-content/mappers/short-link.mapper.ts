import { Injectable } from '@angular/core';
import { LinkDataModel, TextDataModel, ShortLinkContentModel } from '../models';
import { IShortLinkContentResponse, IShortLinkContentLinkDataResponse, IShortLinkContentTextDataResponse } from '../../api/interfaces';
import { LinkType, ShortenTextType } from '../../shared/shared.constants';
import { CenteredPageSize } from '../../ui/ui.constants';

@Injectable()
export class ShortLinkMapper {

    constructor() {}

    map(data: IShortLinkContentResponse): ShortLinkContentModel {
        switch (data.type as LinkType) {
            case LinkType.LINK:
                const linkData = data.data as IShortLinkContentLinkDataResponse;
                return new LinkDataModel({
                    link: linkData.link
                });
            case LinkType.TEXT:
                const textData = data.data as IShortLinkContentTextDataResponse;
                return new TextDataModel({
                    text: textData.text,
                    pageSize: textData.pageSize as CenteredPageSize,
                    textType: textData.textType as ShortenTextType,
                });
        }
    }
}
