import { Component, Input, OnInit } from '@angular/core';
import { TextDataModel } from '../../../../models';
import { CenteredPageSizeService } from '../../../../../ui/services';
import { MarkdownService } from '../../../../../shared/services';
import { ShortenTextType } from '../../../../../shared/shared.constants';

@Component({
    selector: 'app-text-content',
    templateUrl: './text-content.component.html',
    styleUrls: ['./text-content.component.less']
})
export class TextContentComponent implements OnInit {
    @Input() textData!: TextDataModel;

    textTypes: typeof ShortenTextType = ShortenTextType;

    text!: string;

    constructor(
        private centeredPageSizeService: CenteredPageSizeService,
        private markdownService: MarkdownService
    ) {
    }

    ngOnInit(): void {
        this.centeredPageSizeService.setPageSize(this.textData.pageSize);
        switch (this.textData.textType) {
            case ShortenTextType.SIMPLE_TEXT:
                this.text = this.textData.text;
                break;
            case ShortenTextType.MARKDOWN:
                this.text = this.markdownService.convert(this.textData.text);
                break;
        }
    }

}
