import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ShortLinkContentModel } from '../../models';
import { LinkType } from '../../../shared/shared.constants';

@Component({
    selector: 'app-short-link-content',
    templateUrl: './short-link-content.component.html',
    styleUrls: ['./short-link-content.component.less']
})
export class ShortLinkContentComponent implements OnInit {
    shortLinkContent: ShortLinkContentModel;
    linkTypes: typeof LinkType = LinkType;

    constructor(
        private route: ActivatedRoute,
    ) {
        this.shortLinkContent = route.snapshot.data.shortLinkContent;
    }

    ngOnInit(): void {
    }

}
