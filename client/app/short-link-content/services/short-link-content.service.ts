import { Injectable } from '@angular/core';
import { ApiService } from '../../api/services';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ShortLinkMapper } from '../mappers/short-link.mapper';
import { ShortLinkContentModel } from '../models';

@Injectable()
export class ShortLinkContentService {

    constructor(
        private apiService: ApiService,
        private shortLinkMapper: ShortLinkMapper
    ) {
    }

    getLinkContent(code: string): Observable<ShortLinkContentModel> {
        return this.apiService.getLinkContent(code).pipe(
            map(r => this.shortLinkMapper.map(r))
        );
    }
}
