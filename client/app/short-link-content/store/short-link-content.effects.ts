import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
    SHORT_LINK_CONTENT_ACTIONS,
    ShortLinkContentActions,
    shortLinkContentFetchContentError,
    shortLinkContentFetchContentSuccess
} from './short-link-content.actions';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { ShortLinkContentService } from '../services';
import { of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class ShortLinkContentEffects {
    shortLinkContent = createEffect(() => this.actions$.pipe(
        ofType(SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT),
        mergeMap(action => this.shortLinkContentService.getLinkContent(action.code).pipe(
            map(linkContent => shortLinkContentFetchContentSuccess({ content: linkContent })),
            catchError(error => of(shortLinkContentFetchContentError({ error: error })))
        ))
    ));

    constructor(
        private actions$: Actions<ShortLinkContentActions>,
        private shortLinkContentService: ShortLinkContentService,
    ) {
    }
}
