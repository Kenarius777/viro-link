import { ShortLinkContentModel } from '../models';

export const SHORT_LINK_CONTENT_FEATURE_KEY = 'shortLinkContent';

export interface IShortLinkContentState {
    shortLinkContent: ShortLinkContentModel | null;
    error: Error | null;
}

export const SHORT_LINK_CONTENT_DEFAULT_STATE: IShortLinkContentState = {
    shortLinkContent: null,
    error: null
};
