import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IShortLinkContentState, SHORT_LINK_CONTENT_FEATURE_KEY } from './short-link-content.state';

export const shortLinkContentStateSelector = createFeatureSelector<IShortLinkContentState>(SHORT_LINK_CONTENT_FEATURE_KEY);

export const shortLinkContentSelector = createSelector(
    shortLinkContentStateSelector,
    (state: IShortLinkContentState) => state.shortLinkContent
);
