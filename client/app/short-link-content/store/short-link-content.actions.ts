import { createAction, props, union } from '@ngrx/store';
import { ShortLinkContentModel } from '../models';

export enum SHORT_LINK_CONTENT_ACTIONS {
    FETCH_CONTENT = '[SHORT LINK CONTENT] fetch content',
    FETCH_CONTENT_SUCCESS = '[SHORT LINK CONTENT] fetch content success',
    FETCH_CONTENT_ERROR = '[SHORT LINK CONTENT] fetch content error',
}

export const shortLinkContentFetchContent = createAction(
    SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT,
    props<{ code: string }>()
);

export const shortLinkContentFetchContentSuccess = createAction(
    SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_SUCCESS,
    props<{ content: ShortLinkContentModel }>()
);

export const shortLinkContentFetchContentError = createAction(
    SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_ERROR,
    props<{ error: Error }>()
);

const shortLinkContentUnion = union({
    shortLinkContentFetchContent,
    shortLinkContentFetchContentSuccess,
    shortLinkContentFetchContentError,
});

export type ShortLinkContentActions = typeof shortLinkContentUnion;
