import { Action, createReducer, on } from '@ngrx/store';
import { IShortLinkContentState, SHORT_LINK_CONTENT_DEFAULT_STATE } from './short-link-content.state';
import {
    shortLinkContentFetchContent,
    shortLinkContentFetchContentError,
    shortLinkContentFetchContentSuccess
} from './short-link-content.actions';

const reducer = createReducer(
    SHORT_LINK_CONTENT_DEFAULT_STATE,
    on(shortLinkContentFetchContent, (state: IShortLinkContentState) =>
        ({ ...state, shortLinkContent: null, error: null })
    ),
    on(shortLinkContentFetchContentSuccess, (state: IShortLinkContentState, { content }) =>
        ({ ...state, shortLinkContent: content, error: null })
    ),
    on(shortLinkContentFetchContentError, (state: IShortLinkContentState, { error }) =>
        ({ ...state, shortLinkContent: null, error: error })
    )
);

export function shortLinkContentReducer(state: IShortLinkContentState | undefined, action: Action): IShortLinkContentState {
    return reducer(state, action);
}
