import { LinkType } from '../../shared/shared.constants';

export interface IShortLinkContent {
    type: LinkType;
}
