import { CenteredPageSize } from '../../ui/ui.constants';
import { ShortenTextType } from '../../shared/shared.constants';


export interface ITextData {
    text: string;
    pageSize: CenteredPageSize;
    textType: ShortenTextType;
}
