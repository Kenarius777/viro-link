import { ShortLinkContentModel } from './short-link-content.model';
import { ITextData } from '../interfaces';
import { LinkType, ShortenTextType } from '../../shared/shared.constants';
import { CenteredPageSize } from '../../ui/ui.constants';

export class TextDataModel extends ShortLinkContentModel implements ITextData{
    type: LinkType = LinkType.TEXT;
    text: string;
    pageSize: CenteredPageSize;
    textType: ShortenTextType;

    constructor(data: ITextData) {
        super();
        this.text = data.text;
        this.pageSize = data.pageSize;
        this.textType = data.textType;
    }
}
