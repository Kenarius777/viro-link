import { IShortLinkContent } from '../interfaces';
import { LinkType } from '../../shared/shared.constants';

export abstract class ShortLinkContentModel implements IShortLinkContent {
    abstract type: LinkType;

    protected constructor() {
    }
}
