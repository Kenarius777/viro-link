import { ShortLinkContentModel } from './short-link-content.model';
import { ILinkData } from '../interfaces';
import { LinkType } from '../../shared/shared.constants';

export class LinkDataModel extends ShortLinkContentModel implements ILinkData {
    type: LinkType = LinkType.LINK;
    link: string;

    constructor(data: ILinkData) {
        super();
        this.link = data.link;
    }
}
