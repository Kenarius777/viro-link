import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShortLinkContentResolver } from './resolvers';
import { ShortLinkContentExistGuard, ShortLinkContentLoadGuard } from './guards';
import { ShortLinkContentComponent } from './pages/short-link-content/short-link-content.component';

const routes: Routes = [
    {
        path: '',
        component: ShortLinkContentComponent,
        canActivate: [
            ShortLinkContentLoadGuard,
            ShortLinkContentExistGuard,
        ],
        resolve: {
            shortLinkContent: ShortLinkContentResolver
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ShortLinkContentRoutingModule {
}
