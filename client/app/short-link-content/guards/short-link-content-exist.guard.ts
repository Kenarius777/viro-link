import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ShortLinkContentService } from '../services';
import { map } from 'rxjs/operators';
import { LinkDataModel } from '../models';
import { ErrorPageNavigateService, RedirectService } from '../../shared/services';
import { SHORT_LINK_CONTENT_ACTIONS, ShortLinkContentActions } from '../store/short-link-content.actions';
import { Actions, ofType } from '@ngrx/effects';
import { LinkType } from '../../shared/shared.constants';

@Injectable()
export class ShortLinkContentExistGuard implements CanActivate {

    constructor(
        private actions$: Actions<ShortLinkContentActions>,
        private linkService: ShortLinkContentService,
        private redirectService: RedirectService,
        private errorPageNavigateService: ErrorPageNavigateService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.actions$.pipe(
            ofType(SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_SUCCESS, SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_ERROR),
            map(action => {
                switch (action.type) {
                    case SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_SUCCESS:
                        const linkContent = action.content;
                        switch (linkContent.type) {
                            case LinkType.TEXT:
                                return true;
                            case LinkType.LINK:
                                this.redirectService.redirect((linkContent as LinkDataModel).link);
                                return false;
                            default:
                                throw new Error('link type error');
                        }
                    case SHORT_LINK_CONTENT_ACTIONS.FETCH_CONTENT_ERROR:
                        this.errorPageNavigateService.resolveAndNavigate(action.error, state.url);
                        return false;
                }
            })
        );
    }

}
