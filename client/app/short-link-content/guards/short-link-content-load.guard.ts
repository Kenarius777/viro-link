import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { IShortLinkContentState } from '../store/short-link-content.state';
import { Store } from '@ngrx/store';
import { shortLinkContentFetchContent } from '../store/short-link-content.actions';

@Injectable()
export class ShortLinkContentLoadGuard implements CanActivate {

    constructor(
        private store: Store<IShortLinkContentState>
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const linkCode = route.params.code;
        this.store.dispatch(shortLinkContentFetchContent({ code: linkCode }));
        return true;
    }
}
