import { NgModule } from '@angular/core';
import { ShortLinkContentRoutingModule } from './short-link-content-routing.module';
import { ShortLinkContentService } from './services';
import { ShortLinkContentResolver } from './resolvers';
import { ShortLinkContentExistGuard, ShortLinkContentLoadGuard } from './guards';
import { ShortLinkMapper } from './mappers/short-link.mapper';
import { ShortLinkContentComponent } from './pages/short-link-content/short-link-content.component';
import { TextContentComponent } from './pages/short-link-content/components/text-content/text-content.component';
import { CommonModule } from '@angular/common';
import { ApiModule } from '../api/api.module';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { SHORT_LINK_CONTENT_FEATURE_KEY } from './store/short-link-content.state';
import { shortLinkContentReducer } from './store/short-link-content.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ShortLinkContentEffects } from './store/short-link-content.effects';

@NgModule({
    declarations: [
        ShortLinkContentComponent,
        TextContentComponent,
    ],
    imports: [
        ShortLinkContentRoutingModule,
        CommonModule,
        ApiModule,
        SharedModule,
        StoreModule.forFeature(SHORT_LINK_CONTENT_FEATURE_KEY, shortLinkContentReducer),
        EffectsModule.forFeature([
            ShortLinkContentEffects
        ])
    ],
    providers: [
        ShortLinkContentService,
        ShortLinkMapper,
        ShortLinkContentExistGuard,
        ShortLinkContentLoadGuard,
        ShortLinkContentResolver,
    ],
})
export class ShortLinkContentModule {
}
