import { Pipe, PipeTransform } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';

@Pipe({ name: 'newLineToBr' })
export class NewLineToBrPipe implements PipeTransform {

    constructor() { }

    transform(value: string): SafeHtml {
        return value.replace(/\n/g, '<br/>');
    }

}
