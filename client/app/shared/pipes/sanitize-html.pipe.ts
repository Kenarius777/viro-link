import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import DOMPurify from 'dompurify';

@Pipe({ name: 'sanitizeHtml' })
export class SanitizeHtmlPipe implements PipeTransform {

    constructor(private sanitizer: DomSanitizer) { }

    transform(value: string): SafeHtml {
        const sanitizedContent = DOMPurify ? DOMPurify.sanitize(value) : value;
        return this.sanitizer.bypassSecurityTrustHtml(sanitizedContent);
    }

}
