import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Store } from '@ngrx/store';
import { IAppState } from '../../store/app.state';
import { appSetTitle } from '../../store/app.actions';

@Injectable()
export class TitleGuard implements CanActivate, CanActivateChild {

    constructor(
        private store: Store<IAppState>
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const title = route.data.title;
        if (title) {
            this.store.dispatch(appSetTitle({ title }));
        }
        return true;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const title = route.data.title;
        if (title) {
            this.store.dispatch(appSetTitle({ title }));
        }
        return true;
    }
}
