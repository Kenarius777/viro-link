export enum LinkType {
    LINK = 'link',
    TEXT = 'text'
}

export enum ShortenTextType {
    SIMPLE_TEXT = 'text',
    MARKDOWN = 'markdown'
}
