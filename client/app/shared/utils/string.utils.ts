export class StringUtils {
    static padLeft(str: string, size: number, symbol: string = ' '): string {
        let s = str;
        while (s.length < size) {
            s = symbol + s;
        }
        return s;
    }

    static padRight(str: string, size: number, symbol: string = ' '): string {
        let s = str;
        while (s.length < size) {
            s = s + symbol;
        }
        return s;
    }

    static uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            const r = Math.random() * 16 | 0;
            const v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
