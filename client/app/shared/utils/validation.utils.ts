import { AbstractControl, FormControl, ValidationErrors, Validators } from '@angular/forms';

export class SharedValidators {
    static requiredWithoutSpaces(control: AbstractControl): ValidationErrors | null {
        const tempControl = new FormControl();
        tempControl.setValue(control.value ? control.value.trim() : '');
        return Validators.required(tempControl);
    }

    static url(control: AbstractControl): ValidationErrors | null {
        const urlRegex = /^(http(s)?:\/\/.)?(www\.)?[-a-zA-Zа-яА-Я0-9@:%._\+~#=]{2,256}\.[a-zа-я]{2,10}([-a-zA-Zа-яА-Я0-9@:%_\+\/.~#?&=]*)/;
        const isValid = () => urlRegex.test(control.value);
        return !control.value || isValid() ? null : { url: true };
    }
}

export function isValidHttpUrl(value: string) {
    let url;

    try {
        url = new URL(value);
    } catch (_) {
        return false;
    }

    return url.protocol === 'http:' || url.protocol === 'https:';
}
