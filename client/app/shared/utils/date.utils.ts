import { StringUtils } from './string.utils';

export class DateUtils {
    static yyyymmdd(dateIn: Date): string {
        const yyyy = dateIn.getFullYear();
        const mm = StringUtils.padLeft((dateIn.getMonth() + 1).toString(), 2, '0');
        const dd = StringUtils.padLeft((dateIn.getDate()).toString(), 2, '0');
        return yyyy + '-' + mm + '-' + dd;
    }

    static yyyy(dateIn: Date): string {
        const yyyy = dateIn.getFullYear();
        return yyyy.toString();
    }
}
