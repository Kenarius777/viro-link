import { NgModule } from '@angular/core';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { CopyService, ErrorPageNavigateService, MarkdownService, NotifyService, RedirectService } from './services';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NewLineToBrPipe, SanitizeHtmlPipe } from './pipes';
import { MatButtonModule } from '@angular/material/button';
import { ErrorComponent } from './components/error/error.component';
import { TitleGuard } from './guards';

@NgModule({
    declarations: [
        NotFoundComponent,
        ErrorComponent,
        SanitizeHtmlPipe,
        NewLineToBrPipe,
    ],
    imports: [
        RouterModule,
        CommonModule,
        MatSnackBarModule,
        MatButtonModule,
    ],
    providers: [
        RedirectService,
        CopyService,
        NotifyService,
        MarkdownService,
        ErrorPageNavigateService,
        TitleGuard,
    ],
    exports: [
        NotFoundComponent,
        SanitizeHtmlPipe,
        NewLineToBrPipe,
    ]
})
export class SharedModule {
}
