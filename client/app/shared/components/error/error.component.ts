import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'shared-error',
    templateUrl: './error.component.html',
    styleUrls: ['./error.component.less']
})
export class ErrorComponent implements OnInit, OnDestroy {

    private routeReplaced: boolean = false;
    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        private router: Router,
        private location: Location,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        this.initQueryParamListener();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    reloadPage(): void {
        window.location.reload();
    }

    private initQueryParamListener(): void {
        this.route.queryParams.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(queryParams => {
            const routeForReplacement = queryParams.displayRoute;
            if (routeForReplacement) {
                this.routeReplaced = true;
                this.location.replaceState(routeForReplacement);
            }
        });
    }
}
