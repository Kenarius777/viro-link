import { Component, Inject, OnDestroy, OnInit, Optional, PLATFORM_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isPlatformServer, Location } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { RESPONSE } from '@nestjs/ng-universal/dist/tokens';
import { Response } from 'express';

@Component({
    selector: 'shared-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.less']
})
export class NotFoundComponent implements OnInit, OnDestroy {

    private readonly unsubscriber$: Subject<void> = new Subject();

    constructor(
        private router: Router,
        private location: Location,
        private route: ActivatedRoute,
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() @Inject(RESPONSE) private response: Response
    ) {
    }

    ngOnInit(): void {
        if (isPlatformServer(this.platformId) && this.response) {
            this.response.status(404);
        }
        this.initQueryParamListener();
    }

    ngOnDestroy(): void {
        this.unsubscriber$.next();
        this.unsubscriber$.complete();
    }

    private initQueryParamListener(): void {
        this.route.queryParams.pipe(
            takeUntil(this.unsubscriber$)
        ).subscribe(queryParams => {
            const routeForReplacement = queryParams.displayRoute;
            if (routeForReplacement) {
                this.location.replaceState(routeForReplacement);
            }
        });
    }
}
