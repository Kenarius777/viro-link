import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class ErrorPageNavigateService {

    constructor(
        private router: Router
    ) {
    }

    resolveAndNavigate(error: Error, displayRoute?: string | null): void {
        if (error instanceof HttpErrorResponse && error.status === 404) {
            this.navigateTo404Page(displayRoute);
            return;
        }

        this.navigateToErrorPage(displayRoute);
    }

    navigateTo404Page(displayRoute?: string | null): void {
        this.router.navigate(['/404'], {
            queryParams: {
                displayRoute
            }
        });
    }

    navigateToErrorPage(displayRoute?: string | null): void {
        this.router.navigate(['/error'], {
            queryParams: {
                displayRoute
            }
        });
    }
}
