import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class NotifyService {

    private readonly config: MatSnackBarConfig;

    constructor(private matSnackbar: MatSnackBar) {
        this.config = new MatSnackBarConfig();
        this.config.duration = 3000;
        this.config.horizontalPosition = 'end';
        this.config.verticalPosition = 'top';
        this.config.panelClass = ['snack-bar-white'];
    }

    show(message: string, config?: MatSnackBarConfig, action?: string) {
        const nextConfig = config ? Object.assign(this.config, config) : this.config;
        this.matSnackbar.open(message, action, nextConfig);
    }

    showError(error: Error, config: MatSnackBarConfig = { duration: 5000 }, action: string = 'close'): void {
        this.show(this.getErrorMessage(error), config, action);
    }

    private getErrorMessage(error: Error): string {
        if (error instanceof HttpErrorResponse) {
            const errors = error.error.errors;
            if (errors && errors.length > 0) {
                return errors.join(', ');
            }
            return 'An unexpected error has occurred during your request. Please try again later.';
        } else {
            return 'An unexpected error has occurred. Please try again later.';
        }
    }
}
