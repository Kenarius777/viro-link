import { Injectable } from '@angular/core';
import * as marked from 'marked';

@Injectable()
export class MarkdownService {

    constructor() {
    }

    convert(from: string): string {
        return marked(from);
    }
}
