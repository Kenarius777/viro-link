import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';

@Injectable()
export class CopyService {

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        @Inject(DOCUMENT) private document: Document,
    ) {
    }

    copyToClipboard(text: string): boolean {
        if (!isPlatformBrowser(this.platformId)) {
            return false;
        }
        const selBox = this.document.createElement('textarea');
        const range = this.document.createRange();
        selBox.style.position = 'fixed';
        selBox.style.left = '0';
        selBox.style.top = '0';
        selBox.style.opacity = '0';
        selBox.value = text;
        this.document.body.appendChild(selBox);
        selBox.focus();
        selBox.select();
        range.selectNodeContents(selBox);
        const selection = window.getSelection();
        selection?.removeAllRanges();
        selection?.addRange(range);
        selBox.setSelectionRange(0, 999999);
        const successful = this.document.execCommand('copy');
        this.document.body.removeChild(selBox);
        return successful && this.isCopySuccessInIE11(text);
    }

    private isCopySuccessInIE11(text: string): boolean {
        const clipboardData = window['clipboardData'];
        if (clipboardData && clipboardData.getData) {
            if (!clipboardData.getData('Text')) {
                return false;
            }
        }
        return true;
    }
}
