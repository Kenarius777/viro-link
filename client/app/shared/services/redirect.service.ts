import { Inject, Injectable, Optional, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { DOCUMENT } from '@angular/common';
import { Response } from 'express';
import { RESPONSE } from '@nestjs/ng-universal/dist/tokens';

@Injectable()
export class RedirectService {

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() @Inject(RESPONSE) private response: Response,
        @Optional() @Inject(DOCUMENT) private document: Document,
    ) {
    }

    redirect(url: string): void {
        if (isPlatformBrowser(this.platformId)) {
            this.document.location.href = url;
        } else {
            this.response.status(301);
            this.response.setHeader('Location', url);
            this.response.send();
        }
    }
}
