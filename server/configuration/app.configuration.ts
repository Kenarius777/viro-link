import mysqlDatabaseConfig from './mysql.configuration';
import { environment } from '../environments/environment';

export default () => {
    return {
        appUrl: environment.appUrl,
        database: mysqlDatabaseConfig()
    };
};
