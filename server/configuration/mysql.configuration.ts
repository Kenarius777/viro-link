import { ConnectionOptions } from 'typeorm';
import { IS_DEV_MODE } from '../app.constants';
import { ENTITY_LIST } from '../entities';
import { environment } from '../environments/environment';

export default (): ConnectionOptions => {
    return {
        type: 'mysql',
        host: environment.db.host,
        port: environment.db.port,
        username: environment.db.user,
        password: environment.db.password,
        database: environment.db.database,
        entities: ENTITY_LIST,
        synchronize: true,
        dropSchema: false,
        trace: IS_DEV_MODE,
        logging: IS_DEV_MODE,
        cli: {
            entitiesDir: 'server/entities',
            migrationsDir: 'server/migrations',
            subscribersDir: 'server/subscriber',
        },
        timezone: 'Z',
    };
};
