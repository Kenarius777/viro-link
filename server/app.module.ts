import { Module } from '@nestjs/common';
import { AngularUniversalModule } from '@nestjs/ng-universal';
import { join } from 'path';
import { AppServerModule } from '../client/main.server';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration/app.configuration';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShortLinkModule } from './short-link/short-link.module';
import { IS_DEV_MODE } from './app.constants';

@Module({
    imports: [
        ConfigModule.forRoot({
            load: [configuration],
            isGlobal: true,
            envFilePath: [join(process.cwd(), `${process.env.NODE_ENV || ''}.env`)]
        }),
        TypeOrmModule.forRootAsync({
            useFactory: (config: ConfigService) => config.get('database')!,
            inject: [ConfigService]
        }),
        AngularUniversalModule.forRoot({
            bootstrap: AppServerModule,
            viewsPath: join(process.cwd(), IS_DEV_MODE ? 'dist/link-kenarov/browser' : '../browser')
        }),
        ShortLinkModule
    ]
})
export class AppModule {
}
