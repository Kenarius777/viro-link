import { IsAlphanumeric, IsNotEmpty, IsOptional, IsString, IsUrl, MaxLength, MinLength } from 'class-validator';
import { CODE_LENGTH_MIN, CODE_LENGTH_MAX } from '../../app.constants';

export class LinkDataSaveRequest {
    @IsNotEmpty()
    @IsUrl()
    @MaxLength(2048)
    link: string | undefined;

    @IsOptional()
    @MinLength(CODE_LENGTH_MIN)
    @MaxLength(CODE_LENGTH_MAX)
    @IsString()
    @IsAlphanumeric()
    shortCode: string | null | undefined;
}
