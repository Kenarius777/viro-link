export class ShortLinkDataResponse {
    type: string;
    data: ShortLinkDataLink | ShortLinkDataText;

    constructor(type: string, data: ShortLinkDataLink | ShortLinkDataText) {
        this.type = type;
        this.data = data;
    }
}

export class ShortLinkDataLink {
    link: string;

    constructor(link: string) {
        this.link = link;
    }
}

export class ShortLinkDataText {
    text: string;
    textType: string;
    pageSize: string;

    constructor(data: { text: string, textType: string, pageSize: string }) {
        this.text = data.text;
        this.textType = data.textType;
        this.pageSize = data.pageSize;
    }
}
