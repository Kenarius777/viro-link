import { IsAlphanumeric, IsEnum, IsNotEmpty, IsOptional, IsString, IsUrl, MaxLength, MinLength } from 'class-validator';
import { CODE_LENGTH_MIN, CODE_LENGTH_MAX, PageSize, TextType } from '../../app.constants';
import { LinkType } from '../../entities';

export class TextDataSaveRequest {
    @IsNotEmpty()
    @IsString()
    @MaxLength(16000)
    text: string | undefined;

    @IsOptional()
    @MinLength(CODE_LENGTH_MIN)
    @MaxLength(CODE_LENGTH_MAX)
    @IsString()
    @IsAlphanumeric()
    shortCode: string | null | undefined;

    @IsOptional()
    @IsEnum(PageSize)
    pageSize: PageSize | undefined;

    @IsOptional()
    @IsEnum(TextType)
    textType: TextType | undefined;
}
