export class SavedShortLinkResponse {
    code: string;
    shortLink: string;
    deletionKey: string;
    preview: LinkDataPreview | TextDataPreview;

    constructor(
        code: string,
        shortLink: string,
        deletionKey: string,
        preview: LinkDataPreview | TextDataPreview
    ) {
        this.code = code;
        this.shortLink = shortLink;
        this.deletionKey = deletionKey;
        this.preview = preview;
    }
}

export class LinkDataPreview {
    link: string;

    constructor(link: string) {
        this.link = link;
    }
}

export class TextDataPreview {
    text: string;

    constructor(text: string) {
        this.text = text;
    }
}
