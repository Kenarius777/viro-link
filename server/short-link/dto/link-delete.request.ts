import { IsAlphanumeric, IsNotEmpty, IsString } from 'class-validator';


export class LinkDeleteRequest {
    @IsNotEmpty()
    @IsString()
    @IsAlphanumeric()
    key!: string;
}
