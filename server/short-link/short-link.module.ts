import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '../shared/shared.module';
import { ShortLinkController } from './short-link.controller';
import { ShortLinkService } from './short-link.service';
import { ShortLinkEntity } from '../entities';
import {
    DataSaveRequestToEntityMapper,
    EntityToShortLinkDataResponseMapper,
    EntityToSavedShortLinkResponseMapper
} from './mappers';

@Module({
    imports: [
        TypeOrmModule.forFeature([ShortLinkEntity]),
        SharedModule
    ],
    controllers: [ShortLinkController],
    providers: [
        ShortLinkService,
        DataSaveRequestToEntityMapper,
        EntityToSavedShortLinkResponseMapper,
        EntityToShortLinkDataResponseMapper
    ]
})
export class ShortLinkModule {
}
