import { HttpStatus, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ErrorResponse } from '../shared/dto';
import { LinkDataSaveRequest, SavedShortLinkResponse, ShortLinkDataResponse, TextDataSaveRequest } from './dto';
import { CODE_LENGTH_MAX, CODE_LENGTH_MIN, REGENERATE_ATTEMPTS } from '../app.constants';
import to from 'await-to-js';
import { StringService } from '../shared/services';
import { ShortLinkEntity } from '../entities';
import {
    DataSaveRequestToEntityMapper,
    EntityToSavedShortLinkResponseMapper,
    EntityToShortLinkDataResponseMapper
} from './mappers';

@Injectable()
export class ShortLinkService {
    constructor(
        @InjectRepository(ShortLinkEntity)
        private linkRepository: Repository<ShortLinkEntity>,
        private dataSaveRequestToEntityMapper: DataSaveRequestToEntityMapper,
        private entityToShortLinkDataResponseMapper: EntityToShortLinkDataResponseMapper,
        private entityToSavedShortLinkResponseMapper: EntityToSavedShortLinkResponseMapper,
        private stringService: StringService
    ) {
    }

    async findLink(code: string): Promise<ShortLinkDataResponse> {
        const linkEntity = await this.linkRepository.findOne({ code: code }, {relations: ['linkData', 'textData']});
        if (linkEntity === undefined) {
            throw new ErrorResponse([
                'Link not founded'
            ], HttpStatus.NOT_FOUND);
        }
        this.linkRepository.increment({ code: code }, 'requestCounter', 1);
        return this.entityToShortLinkDataResponseMapper.map(linkEntity);
    }

    async deleteLink(code: string, deletionKey: string): Promise<boolean> {
        if (!await this.isLinkExist(code)) {
            return true;
        }
        const [error, result] = await to(this.linkRepository.delete({code: code, deletionKey: deletionKey}));
        if (error || result === undefined) {
            throw new ErrorResponse('db delete error');
        }
        return true;
    }

    private async trySaveLink(linkEntity: Partial<ShortLinkEntity>): Promise<ShortLinkEntity> {
        if (linkEntity.code === null || linkEntity.code === undefined) {
            linkEntity.code = await this.generateLinkShortCode();
        } else {
            if (await this.isLinkExist(linkEntity.code)) {
                throw new ErrorResponse(['Selected short code already exist']);
            }
        }
        return this.saveLink(linkEntity);
    }

    private async saveLink(linkEntity: Partial<ShortLinkEntity>): Promise<ShortLinkEntity> {
        const [error, result] = await to(this.linkRepository.save(linkEntity));
        if (error || result === undefined) {
            throw new ErrorResponse('db save error');
        }
        return result;
    }

    private async isLinkExist(code: string): Promise<boolean> {
        const [error, count] = await to(this.linkRepository.count({ code: code }));
        if (error || count === undefined) {
            throw new ErrorResponse('db link check error');
        }
        return count > 0;
    }

    private async generateLinkShortCode(): Promise<string> {
        let linkSize = CODE_LENGTH_MIN;
        let regenerateAttemptsCounter = 0;
        let link = this.stringService.generateString(linkSize);
        while (await this.isLinkExist(link)) {
            if (linkSize > CODE_LENGTH_MAX) {
                throw new ErrorResponse('generate link error');
            }
            if (regenerateAttemptsCounter < REGENERATE_ATTEMPTS) {
                link = this.stringService.generateString(linkSize);
                regenerateAttemptsCounter++;
            } else {
                regenerateAttemptsCounter = 0;
                linkSize++;
            }
        }
        return link;
    }

    async saveUserTextData(request: TextDataSaveRequest): Promise<SavedShortLinkResponse> {
        const linkEntity = this.dataSaveRequestToEntityMapper.mapTextDataSaveRequestToEntity(request);
        const savedLinkEntity = await this.trySaveLink(linkEntity);
        return this.entityToSavedShortLinkResponseMapper.map(savedLinkEntity);
    }

    async saveUserLinkData(request: LinkDataSaveRequest): Promise<SavedShortLinkResponse> {
        const linkEntity = this.dataSaveRequestToEntityMapper.mapLinkDataSaveRequestToEntity(request);
        const savedLinkEntity = await this.trySaveLink(linkEntity);
        return this.entityToSavedShortLinkResponseMapper.map(savedLinkEntity);
    }
}
