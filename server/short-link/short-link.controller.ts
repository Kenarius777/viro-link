import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { LinkDataSaveRequest, LinkDeleteRequest, SavedShortLinkResponse, ShortLinkDataResponse, TextDataSaveRequest } from './dto';
import { ShortLinkService } from './short-link.service';

@Controller('short-link')
export class ShortLinkController {
    constructor(
        private linkService: ShortLinkService
    ) {
    }

    @Get(':code')
    async getShortLink(@Param('code') code: string): Promise<ShortLinkDataResponse> {
        return this.linkService.findLink(code);
    }

    @Delete(':code')
    async deleteShortLink(@Param('code') code: string, @Body() request: LinkDeleteRequest): Promise<boolean> {
        return this.linkService.deleteLink(code, request.key);
    }

    @Post('text/save')
    async saveText(@Body() request: TextDataSaveRequest): Promise<SavedShortLinkResponse> {
        return this.linkService.saveUserTextData(request);
    }

    @Post('link/save')
    async saveLink(@Body() request: LinkDataSaveRequest): Promise<SavedShortLinkResponse> {
        return this.linkService.saveUserLinkData(request);
    }
}
