import { Injectable } from '@nestjs/common';
import { LinkDataEntity, LinkType, ShortLinkEntity, TextDataEntity } from '../../entities';
import {
    ShortLinkDataLink,
    ShortLinkDataResponse,
    ShortLinkDataText
} from '../dto';

@Injectable()
export class EntityToShortLinkDataResponseMapper {
    constructor() {
    }

    map(linkEntity: ShortLinkEntity): ShortLinkDataResponse {
        let data: ShortLinkDataLink | ShortLinkDataText;
        switch (linkEntity.type) {
            case LinkType.LINK:
                data = this.mapEntityToShortLinkDataLink(linkEntity.linkData);
                break;
            case LinkType.TEXT:
                data = this.mapEntityToShortLinkDataText(linkEntity.textData);
                break;
        }
        return new ShortLinkDataResponse(linkEntity.type, data);
    }
    private mapEntityToShortLinkDataLink(dataEntity: LinkDataEntity): ShortLinkDataLink {
        return new ShortLinkDataLink(dataEntity.link);
    }

    private mapEntityToShortLinkDataText(dataEntity: TextDataEntity): ShortLinkDataText {
        return new ShortLinkDataText({
            text: dataEntity.text,
            pageSize: dataEntity.pageSize,
            textType: dataEntity.textType
        });
    }
}
