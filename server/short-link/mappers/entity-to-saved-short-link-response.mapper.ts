import { Injectable } from '@nestjs/common';
import { LinkType, ShortLinkEntity } from '../../entities';
import {
    LinkDataPreview,
    SavedShortLinkResponse,
    TextDataPreview,
} from '../dto';
import { environment } from '../../environments/environment';

@Injectable()
export class EntityToSavedShortLinkResponseMapper {
    constructor() {
    }

    map(linkEntity: ShortLinkEntity): SavedShortLinkResponse {
        let dataPreview: LinkDataPreview | TextDataPreview;
        switch (linkEntity.type) {
            case LinkType.LINK:
                dataPreview = new LinkDataPreview(linkEntity.linkData.link);
                break;
            case LinkType.TEXT:
                if (linkEntity.textData.text.length > 150) {
                    dataPreview = new TextDataPreview(linkEntity.textData.text.slice(0, 150) + '...');
                } else {
                    dataPreview = new TextDataPreview(linkEntity.textData.text);
                }
                break;
        }
        return new SavedShortLinkResponse(
            linkEntity.code,
            [environment.appUrl, linkEntity.code].join('/'),
            linkEntity.deletionKey,
            dataPreview
        );
    }
}
