import { Injectable } from '@nestjs/common';
import { LinkDataEntity, LinkType, ShortLinkEntity, TextDataEntity } from '../../entities';
import {
    LinkDataSaveRequest,
    TextDataSaveRequest
} from '../dto';
import { StringService } from '../../shared/services';
import { DEFAULT_PAGE_SIZE, DEFAULT_TEXT_TYPE, DELETION_KEY_LENGTH } from '../../app.constants';

@Injectable()
export class DataSaveRequestToEntityMapper {
    constructor(
        private stringService: StringService
    ) {
    }

    mapTextDataSaveRequestToEntity(request: TextDataSaveRequest): Partial<ShortLinkEntity> {
        const textDataEntity = new TextDataEntity({
            text: request.text,
            pageSize: request.pageSize || DEFAULT_PAGE_SIZE,
            textType: request.textType || DEFAULT_TEXT_TYPE
        });
        const shortLinkEntity = this.mapSaveRequestToShortLinkEntity(request);
        shortLinkEntity.textData = textDataEntity;
        shortLinkEntity.type = LinkType.TEXT;
        return shortLinkEntity;
    }

    mapLinkDataSaveRequestToEntity(request: LinkDataSaveRequest): Partial<ShortLinkEntity> {
        let link = request.link!;
        if (!(link.startsWith('http://') || link.startsWith('https://'))) {
            link = `https://${link}`;
        }
        const linkDataEntity = new LinkDataEntity({
            link: link
        });
        const shortLinkEntity = this.mapSaveRequestToShortLinkEntity(request);
        shortLinkEntity.linkData = linkDataEntity;
        shortLinkEntity.type = LinkType.LINK;
        return shortLinkEntity;
    }

    private mapSaveRequestToShortLinkEntity(request: LinkDataSaveRequest | TextDataSaveRequest): Partial<ShortLinkEntity> {
        const shortLinkEntity = new ShortLinkEntity();
        if (request.shortCode !== null && request.shortCode !== undefined) {
            shortLinkEntity.code = request.shortCode;
        }
        shortLinkEntity.deletionKey = this.stringService.generateString(DELETION_KEY_LENGTH);
        return shortLinkEntity;
    }
}
