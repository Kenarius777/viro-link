import { Injectable } from '@nestjs/common';

@Injectable()
export class StringService {
    constructor() {
    }

    generateString(length: number): string {
        const result: Array<String> = [];
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
            result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
        }
        return result.join('');
    }
}
