import { Module } from '@nestjs/common';
import { StringService } from './services';

@Module({
    imports: [],
    providers: [
        StringService,
    ],
    exports: [
        StringService
    ]
})
export class SharedModule {}
