import { HttpException, HttpStatus } from '@nestjs/common';

export class ErrorResponse extends HttpException {
    errors: Array<string>;
    stacktrace: string | null = null;

    constructor(errors: Array<string> | string, statusCode: number = HttpStatus.BAD_REQUEST) {
        super(errors, statusCode);
        this.errors = Array.isArray(errors) ? errors : [errors];
    }

    getResponse(): string | object {
        return {
            statusCode: super.getStatus(),
            errors: this.errors,
            stacktrace: this.stacktrace
        };
    }
}
