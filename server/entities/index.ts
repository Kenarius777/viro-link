import { ShortLinkEntity } from './short-link.entity';
import { LinkDataEntity } from './link-data.entity';
import { TextDataEntity } from './text-data.entity';

export * from './short-link.entity';
export * from './link-data.entity';
export * from './text-data.entity';

export const ENTITY_LIST: Array<Function> = [
    ShortLinkEntity,
    LinkDataEntity,
    TextDataEntity,
];
