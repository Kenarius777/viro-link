import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { JoinColumn } from 'typeorm/browser';
import { ShortLinkEntity } from './short-link.entity';
import { PageSize, TextType } from '../app.constants';

@Entity('text-data')
export class TextDataEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: false, type: 'text' })
    text!: string;

    @Column({ nullable: false })
    pageSize!: PageSize;

    @Column({ nullable: false })
    textType!: TextType;

    @OneToOne(type => ShortLinkEntity, { onDelete: 'CASCADE' })
    @JoinColumn()
    shortLink!: ShortLinkEntity;

    constructor(data?: {
        text: string | undefined,
        pageSize: PageSize | undefined,
        textType: TextType | undefined
    } | null | undefined) {
        if (data) {
            Object.assign(this, data);
        }
    }
}
