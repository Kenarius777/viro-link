import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { JoinColumn } from 'typeorm/browser';
import { ShortLinkEntity } from './short-link.entity';

@Entity('link-data')
export class LinkDataEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column({ nullable: false, type: 'text' })
    link!: string;

    @OneToOne(type => ShortLinkEntity, { onDelete: 'CASCADE' })
    @JoinColumn({name: 'short_link_id'})
    shortLink!: ShortLinkEntity;

    constructor(data?: {
        link: string | undefined,
    } | null | undefined) {
        if (data) {
            Object.assign(this, data);
        }
    }
}
