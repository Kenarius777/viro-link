import { Column, CreateDateColumn, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { CODE_LENGTH_MAX, DELETION_KEY_LENGTH } from '../app.constants';
import { LinkDataEntity } from './link-data.entity';
import { TextDataEntity } from './text-data.entity';

@Entity('short-links')
export class ShortLinkEntity {
    @PrimaryGeneratedColumn()
    id!: number;

    @CreateDateColumn()
    createdAt!: Date;

    @Column({ nullable: false, length: CODE_LENGTH_MAX, unique: true })
    code!: string;

    @Column({ nullable: false, length: 10 })
    type!: LinkType;

    @Column({ nullable: false, default: 0 })
    requestCounter!: number;

    @Column({ nullable: false, length: DELETION_KEY_LENGTH })
    deletionKey!: string;

    @OneToOne(type => LinkDataEntity, linkDataEntity => linkDataEntity.shortLink, { cascade: true })
    linkData!: LinkDataEntity;

    @OneToOne(type => TextDataEntity, textDataEntity => textDataEntity.shortLink, { cascade: true })
    textData!: TextDataEntity;

    constructor(data?: {
        id: number | undefined,
        createdAt: Date | undefined,
        code: string | undefined,
        type: LinkType | undefined,
        data: string | undefined,
        requestCounter: number | undefined,
        deletionKey: string | undefined,
    } | null | undefined) {
        if (data) {
            Object.assign(this, data);
        }
    }
}

export enum LinkType {
    LINK = 'link',
    TEXT = 'text'
}
