import { environment } from './environments/environment';

export const IS_DEV_MODE: boolean = !environment.production;

export const CODE_LENGTH_MIN: number = 3;
export const CODE_LENGTH_MAX: number = 64;
export const REGENERATE_ATTEMPTS: number = 10;
export const DELETION_KEY_LENGTH: number = 48;

export enum PageSize {
    SMALL = 'small',
    MEDIUM = 'medium',
    LARGE = 'large'
}

export enum TextType {
    SIMPLE_TEXT = 'text',
    MARKDOWN = 'markdown'
}

export const DEFAULT_PAGE_SIZE: PageSize = PageSize.SMALL;
export const DEFAULT_TEXT_TYPE: TextType = TextType.SIMPLE_TEXT;


