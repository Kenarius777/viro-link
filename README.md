# ViroLink

Link shortening service (backend NestJS + frontend Angular)

## Dependencies

`npm i`

## Development server

To configure the database (MySQL by default), you need to edit the configuration file at  `server/environments/environment.ts`

To start the development server, run `npm run dev:ssr`. The application will start by default at `http://localhost:4200/`.

## Demo

https://viro.link
